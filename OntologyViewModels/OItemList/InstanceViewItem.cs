﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class InstanceViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        public string IdInstance
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(PropertyName.OItemList_Instance_IdInstance);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0)]
        public string NameInstance
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(PropertyName.OItemList_Instance_NameInstance);
            }
        }

        public string IdClass
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_Instance_IdClass);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1)]
        public string NameClass
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_Instance_NameClass);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClass(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        public InstanceViewItem(clsOntologyItem proxyItem, clsOntologyItem classItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = classItem.Name;
        }
    }

   
}
