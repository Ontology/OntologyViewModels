﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ObjectObject_Conscious : ObjectRelationViewItem
    {
        
        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Name_Object);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 2)]
        public string NameOther
        {
            get { return proxyItem.Name_Other; }
            set
            {
                proxyItem.Name_Other = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Name_Other);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 1)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_RelationType = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Name_RelationType);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 3)]
        public string NameParentOther
        {
            get { return proxyItem.Name_Parent_Other; }
            set
            {
                proxyItem.Name_Parent_Other = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Name_Parent_Other);
            }
        }

        public string NameParentObject
        {
            get { return proxyItem.Name_Parent_Object; }
            set
            {
                proxyItem.Name_Parent_Object = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Name_Parent_Object);
            }
        }

        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 0)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_OrderID);
            }
        }

        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 4)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_Ontology);
            }
        }

        

        public ObjectObject_Conscious(clsObjectRel proxyItem) : base(proxyItem)
        {

        }
    }
}
