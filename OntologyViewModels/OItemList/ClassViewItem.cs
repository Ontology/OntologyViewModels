﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ClassViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        public string IdClass
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(PropertyName.OItemList_Class_IdClass);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0)]
        public string NameClass
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(PropertyName.OItemList_Class_NameClass);
            }
        }

        public string IdParentClass
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_Class_IdParentClass);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1)]
        public string NameParentClass
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_Class_NameParentClass);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClass(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        public ClassViewItem(clsOntologyItem proxyItem, clsOntologyItem parentClass)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = parentClass.Name;
        }
    }
}
