﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class AttributeTypeViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        public string IdAttributeType
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(PropertyName.OItemList_AttributeType_IdAttributeType);
            }
        }

        [DataViewColumn(IsVisible =true, DisplayOrder = 0)]
        public string NameAttributeType
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(PropertyName.OItemList_AttributeType_NameAttributeType);
            }
        }

        public string IdDataType
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_AttributeType_IdDataType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1)]
        public string NameDataType
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(PropertyName.OItemList_AttributeType_NameDataType);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveAttributeTypes(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        
        public AttributeTypeViewItem(clsOntologyItem proxyItem, clsOntologyItem dataTypeItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = dataTypeItem.Name;
        }
    }
}
