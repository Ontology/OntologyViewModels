﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ClassAttributeViewItem : NotifyPropertyChange
    {

        private clsClassAtt proxyItem;

        public string IdClass
        {
            get { return proxyItem.ID_Class; }
            set
            {
                proxyItem.ID_Class = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_IdClass);
            }
        }

        public string NameClass
        {
            get { return proxyItem.Name_Class; }
            set
            {
                proxyItem.Name_Class = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_NameClass);
            }
        }

        public string IdAttributeType
        {
            get { return proxyItem.ID_AttributeType; }
            set
            {
                proxyItem.ID_AttributeType = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_IdAttributeType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0)]
        public string NameAttributeType
        {
            get { return proxyItem.Name_AttributeType; }
            set
            {
                proxyItem.Name_AttributeType = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_NameAttributeType);
            }
        }

        public string IdDataType
        {
            get { return proxyItem.ID_DataType; }
            set
            {
                proxyItem.ID_DataType = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_IdDataType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1)]
        public string NameDataTpye
        {
            get { return proxyItem.Name_DataType; }
            set
            {
                proxyItem.Name_DataType = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_NameDataTpye);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 2)]
        public long Min
        {
            get { return proxyItem.Min ?? 0; }
            set
            {
                proxyItem.Min = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_Min);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 4)]
        public long Max
        {
            get { return proxyItem.Max ?? 0; }
            set
            {
                proxyItem.Max = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassAtt_Max);
            }
        }

        public clsClassAtt GetProxyItem()
        {
            return proxyItem;
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClassAtt(new List<clsClassAtt> { proxyItem });

            return result;
        }

        public ClassAttributeViewItem(clsClassAtt proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
