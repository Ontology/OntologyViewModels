﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ClassRelationViewItem : NotifyPropertyChange
    {
        internal clsClassRel proxyItem;

        public string IdClassLeft
        {
            get { return proxyItem.ID_Class_Left; }
            set
            {
                proxyItem.ID_Class_Left = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_IdClassLeft);
            }
        }

        public string IdClassRight
        {
            get { return proxyItem.ID_Class_Right; }
            set
            {
                proxyItem.ID_Class_Left = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_IdClassRight);
            }
        }

        public string IdRelationType
        {
            get { return proxyItem.ID_RelationType; }
            set
            {
                proxyItem.ID_RelationType = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_IdRelationType);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClassRel(new List<clsClassRel> { proxyItem });

            return result;
        }

        public ClassRelationViewItem(clsClassRel proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
