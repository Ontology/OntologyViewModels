﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.DataAdapter;
using OntologyViewModels.DataGridViewWinForms;
using Structure_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OntologyViewModels.OItemList
{
    [Flags]
    public enum OItemListViewModelErrorType
    {
        None = 0,
        WrongItemType = 1,
        SelectionCountError = 2,
        SaveError = 4,
        MinChangeNotValid = 8,
        MaxChangeNotValid = 16,
        DeleteError = 32
    }
    public class ViewModelWinFormsOItemList : ViewModelOItemList
    {

        private clsOntologyItem FilterItem_Note;
        private clsClassAtt filterItem_ClassAtt;
        private clsClassRel filterItem_ClassRel;
        private clsObjectAtt filterItem_ObjectAtt;
        private clsObjectRel filterItem_ObjectRel;

        private object selectedItem;

        private OItemListViewModelErrorType viewModelErrorType;
        public OItemListViewModelErrorType ViewModelErrorType
        {
            get { return viewModelErrorType; }
            set
            {
                viewModelErrorType = value;
                RaisePropertyChanged(PropertyName.OItemList_ViewModelErrorType);
            }
        }

        private DataGridView dataGridView_WinForms;
        public DataGridView DataGridView_WinForms
        {
            get { return dataGridView_WinForms; }
            set
            {
                dataGridView_WinForms = value;
            }
        }

        private int countItems;
        public int CountItems
        {
            get { return countItems; }
            set
            {
                countItems = value;
                RaisePropertyChanged(PropertyName.OItemList_CountItems);
            }
        }

        private string idSelected;
        public string IdSelected
        {
            get { return idSelected; }
            set
            {
                idSelected = value;
                RaisePropertyChanged(PropertyName.OItemList_IdSelected);
            }
        }

        public ViewModelWinFormsOItemList(Globals globals) : base(globals)
        {
            ListAdapter.PropertyChanged += ListAdapter_PropertyChanged;
        }


        private bool delAllowed;
        public bool DelAllowed
        {
            get { return delAllowed; }
            set
            {
                delAllowed = value;
                RaisePropertyChanged(PropertyName.OItemList_DelAllowed);
            }
        }

        public void WinForms_Initialize_NoteListForGridView(ListType listType, clsOntologyItem filterItem, DataGridView dataGridView_WinForms, bool exact = false)
        {
            DataGridView_WinForms = dataGridView_WinForms;
            Initialize_ItemList(listType, filterItem, exact, DestinationType.WinForms);

        }

        public void WinForms_Initialize_ClassAttListForGridView(clsClassAtt filterItem, DataGridView dataGridView_WinForms, bool exact = false)
        {
            DataGridView_WinForms = dataGridView_WinForms;
            Initialize_ItemList(filterItem, exact, DestinationType.WinForms);

        }

        public void WinForms_Initialize_ClassRelListForGridView(ListType listType, clsClassRel filterItem, DataGridView dataGridView_WinForms, bool exact = false)
        {
            DataGridView_WinForms = dataGridView_WinForms;
            Initialize_ItemList(listType, filterItem, exact, DestinationType.WinForms);

        }

        public void WinForms_Initialize_ObjectAttListForGridView(clsObjectAtt filterItem, DataGridView dataGridView_WinForms, bool exact = false)
        {
            DataGridView_WinForms = dataGridView_WinForms;
            Initialize_ItemList(filterItem, exact, DestinationType.WinForms);

        }

        public void WinForms_Initialize_ObjectRelListForGridView(ListType listType, clsObjectRel filterItem, DataGridView dataGridView_WinForms, bool exact = false)
        {
            DataGridView_WinForms = dataGridView_WinForms;
            Initialize_ItemList(listType, filterItem, exact, DestinationType.WinForms);

        }

        public bool IsAllowedMinTo0
        {
            get
            {
                return IsAllowedMinDecrease;
            }
        }

        private bool isAllowedMinIncrease;
        public bool IsAllowedMinIncrease
        {
            get
            {
                return isAllowedMinIncrease;
                
            }
            set
            {
                isAllowedMinIncrease = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMinIncrease);
            }
        }

        private bool isAllowedMinDecrease;
        public bool IsAllowedMinDecrease
        {
            get
            {
                return isAllowedMinDecrease;
                
            }
            set
            {
                isAllowedMinDecrease = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMinDecrease);
            }
        }

        private bool isAllowedMaxIncrease;
        public bool IsAllowedMaxIncrease
        {
            get
            {
                return isAllowedMaxIncrease;
                
            }
            set
            {
                isAllowedMaxIncrease = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMaxIncrease);
            }
        }
        
        private bool isAllowedMaxEdit;
        public bool IsAllowedMaxEdit
        {
            get
            {
                return isAllowedMaxEdit;
                
            }
            set
            {
                isAllowedMaxEdit = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMaxEdit);
            }
        }

        private bool isAllowedMaxBackwEdit;
        public bool IsAllowedMaxBackwEdit
        {
            get
            {
                return isAllowedMaxBackwEdit;

            }
            set
            {
                isAllowedMaxBackwEdit = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMaxBackwEdit);
            }
        }

        private bool isAllowedMinEdit;
        public bool IsAllowedMinEdit
        {
            get
            {
                return isAllowedMinEdit;
                
            }
            set
            {
                isAllowedMinEdit = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMinEdit);
            }
        }

        private bool isAllowedMaxDecrease;
        public bool IsAllowedMaxDecrease
        {
            get
            {
                return isAllowedMaxDecrease;
                
                
            }
            set
            {
                isAllowedMaxDecrease = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMaxDecrease);
            }
        }

        private bool isAllowedMaxToInvinite;
        public bool IsAllowedMaxToInvinite
        {
            get
            {
                return isAllowedMaxToInvinite;
                
            }
            set
            {
                isAllowedMaxToInvinite = value;
                RaisePropertyChanged(PropertyName.OItemList_IsAllowedMaxToInvinite);
            }
        }

        public bool IsAllowedChangeOrderId
        {
            get
            {
                if (IsAllowedChangeOrderIdForListType)
                {
                    if (dataGridView_WinForms.SelectedRows.Count == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsEnabled_MoveUp
        {
            get
            {
                if ( DataGridView_WinForms.SelectedRows.Count == 1)
                {
                    return (DataGridView_WinForms.SelectedRows[0].Index > 0);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsEnabled_MoveDown
        {
            get
            {
                if (DataGridView_WinForms.SelectedRows.Count == 1)
                {
                    return (DataGridView_WinForms.SelectedRows[0].Index < DataGridView_WinForms.RowCount-1);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsEnabled_Reorder
        {
            get
            {
                return dataGridView_WinForms.SelectedRows.Count > 1;
            }
        }

        public long GetOrderIdOfItem(object item)
        {
            long orderId = 0;
            switch (ListType)
            {
                case ListType.InstanceAttribute_Conscious:
                    orderId = ((ObjectAttributeViewItem)item).OrderId;
                    
                    break;
                case ListType.InstanceInstance_Conscious:
                    orderId = ((ObjectObject_Conscious)item).OrderId;

                    break;
                case ListType.InstanceInstance_Subconscious:
                    orderId = ((ObjectObject_Subconscious)item).OrderId;
                    
                    break;
                case ListType.InstanceInstance_Omni:
                    orderId = ((ObjectObject_Omni)item).OrderId;
                    
                    break;
                case ListType.InstanceOther_Conscious:
                    orderId = ((ObjectObject_Conscious)item).OrderId;

                    break;
                case ListType.InstanceOther_Subconscious:
                    orderId = ((ObjectObject_Subconscious)item).OrderId;

                    break;
                default:

                    break;
            }

            return orderId;
        }

        public clsOntologyItem SaveNewOrderIdOfItems(List<object> items, long orderId)
        {
            var result = globals.LState_Error.Clone();
            switch (ListType)
            {
                case ListType.InstanceAttribute_Conscious:
                    var objectAttributes = items.Select(itm => ((ObjectAttributeViewItem)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectAtt>().ToList();
                    result = ListAdapter.SaveObjectAttributes(objectAttributes);
                    break;
                case ListType.InstanceInstance_Conscious:
                    var objectObjectConscious = items.Select(itm => ((ObjectObject_Conscious)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectRel>().ToList();
                    result = ListAdapter.SaveObjectRelations(objectObjectConscious);

                    break;
                case ListType.InstanceInstance_Subconscious:
                    var objectObjectSubconscious = items.Select(itm => ((ObjectObject_Subconscious)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectRel>().ToList();
                    result = ListAdapter.SaveObjectRelations(objectObjectSubconscious);

                    break;
                case ListType.InstanceInstance_Omni:
                    var objectObjectOmni = items.Select(itm => ((ObjectObject_Omni)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectRel>().ToList();
                    result = ListAdapter.SaveObjectRelations(objectObjectOmni);

                    break;
                case ListType.InstanceOther_Conscious:
                    var objectOther_Conscious = items.Select(itm => ((ObjectObject_Conscious)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectRel>().ToList();
                    result = ListAdapter.SaveObjectRelations(objectOther_Conscious);
                    

                    break;
                case ListType.InstanceOther_Subconscious:
                    var objectOther_Subconscious = items.Select(itm => ((ObjectObject_Subconscious)itm).GetProxyItem().OrderID = orderId++).Cast<clsObjectRel>().ToList();
                    result = ListAdapter.SaveObjectRelations(objectOther_Subconscious);

                    break;
                default:

                    break;
            }

            return result;
        }

        private clsOntologyItem FillDataControl(string propertyName)
        {
            var result = globals.LState_Success.Clone();
            if (DestinationType == DestinationType.WinForms)
            {
                if (propertyName == PropertyName.OItemList_ListAdapter_Result_ListLoader)
                {
                    DataGridView_WinForms.DataSource = null;
                    if (ListAdapter.Result_ListLoader.GUID == globals.LState_Success.GUID)
                    {
                        switch (ListType)
                        {
                            case ListType.AttributeTypes:
                                var dataGridViewAdapterAttributeTypes = new DataGridViewAdapter<AttributeTypeViewItem>(DataGridView_WinForms, new SortableBindingList<AttributeTypeViewItem>(ListAdapter.AttributeTypeViewItems), ListType.AttributeTypes);
                                AddAllowed = true;
                                break;
                            case ListType.Classes:
                                var dataGridViewAdapterClasses = new DataGridViewAdapter<ClassViewItem>(DataGridView_WinForms, new SortableBindingList<ClassViewItem>(ListAdapter.ClassViewItems), ListType.Classes);
                                AddAllowed = true;
                                break;
                            case ListType.RelationTypes:
                                var dataGridViewAdapterRelationTypes = new DataGridViewAdapter<RelationTypeViewItem>(DataGridView_WinForms, new SortableBindingList<RelationTypeViewItem>(ListAdapter.RelationTypeViewItems), ListType.RelationTypes);
                                AddAllowed = true;
                                break;
                            case ListType.Instances:
                                var dataGridViewAdapterInstances = new DataGridViewAdapter<InstanceViewItem>(DataGridView_WinForms, new SortableBindingList<InstanceViewItem>(ListAdapter.InstanceViewItems), ListType.Instances);
                                AddAllowed = true;
                                break;
                            case ListType.ClassAttributeRelation:
                                var dataGridViewAdapterClassAtts = new DataGridViewAdapter<ClassAttributeViewItem>(DataGridView_WinForms, new SortableBindingList<ClassAttributeViewItem>(ListAdapter.ClassAttViewItems), ListType.ClassAttributeRelation);
                                AddAllowed = true;
                                break;
                            case ListType.ClassClassRelation_Conscious:
                                var dataGridViewAdapterClassRelsConscious = new DataGridViewAdapter<ClassRelationConsciousViewItem>(DataGridView_WinForms, new SortableBindingList<ClassRelationConsciousViewItem>(ListAdapter.ClassRelConciousViewItems), ListType.ClassClassRelation_Conscious);
                                AddAllowed = true;
                                break;
                            case ListType.ClassClassRelation_Subconscious:
                                var dataGridViewAdapterClassRelsSubconscious = new DataGridViewAdapter<ClassRelationSubconsciousViewItem>(DataGridView_WinForms, new SortableBindingList<ClassRelationSubconsciousViewItem>(ListAdapter.ClassRelSubconsciousViewItems), ListType.ClassClassRelation_Subconscious);
                                AddAllowed = true;
                                break;
                            case ListType.ClassClassRelation_Omni:
                                var dataGridViewAdapterClassRelsOmni = new DataGridViewAdapter<ClassRelationOmniViewItem>(DataGridView_WinForms, new SortableBindingList<ClassRelationOmniViewItem>(ListAdapter.ClassRelOmniViewItems), ListType.ClassClassRelation_Omni);
                                AddAllowed = true;
                                break;
                            case ListType.ClassOtherRelation_Conscious:
                                var dataGridViewAdapterClassRelsOther = new DataGridViewAdapter<ClassRelationConsciousViewItem>(DataGridView_WinForms, new SortableBindingList<ClassRelationConsciousViewItem>(ListAdapter.ClassRelConciousViewItems), ListType.ClassOtherRelation_Conscious);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceAttribute_Conscious:
                                var dataGridViewAdapterObjectAttribute = new DataGridViewAdapter<ObjectAttributeViewItem>(DataGridView_WinForms, new SortableBindingList<ObjectAttributeViewItem>(ListAdapter.ObjectAttributeViewItems), ListType.InstanceAttribute_Conscious);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceInstance_Conscious:
                                var dataGridViewAdapterObjectRelationConscious = new DataGridViewAdapter<ObjectObject_Conscious>(DataGridView_WinForms, new SortableBindingList<ObjectObject_Conscious>(ListAdapter.ObjectObjectConsciousViewItems), ListType.InstanceInstance_Conscious);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceInstance_Subconscious:
                                var dataGridViewAdapterObjectRelationSubconscious = new DataGridViewAdapter<ObjectObject_Subconscious>(DataGridView_WinForms, new SortableBindingList<ObjectObject_Subconscious>(ListAdapter.ObjectObjectSubconsciousViewItems), ListType.InstanceInstance_Subconscious);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceInstance_Omni:
                                var dataGridViewAdapterObjectRelationOmni = new DataGridViewAdapter<ObjectObject_Omni>(DataGridView_WinForms, new SortableBindingList<ObjectObject_Omni>(ListAdapter.ObjectObjectOmniViewItems), ListType.InstanceInstance_Omni);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceOther_Conscious:
                                var dataGridViewAdapterObjectOtherConscious = new DataGridViewAdapter<ObjectObject_Conscious>(DataGridView_WinForms, new SortableBindingList<ObjectObject_Conscious>(ListAdapter.ObjectOtherConsciousViewItems), ListType.InstanceOther_Conscious);
                                AddAllowed = true;
                                break;
                            case ListType.InstanceOther_Subconscious:
                                var dataGridViewAdapterObjectOtherSubconscious = new DataGridViewAdapter<ObjectObject_Subconscious>(DataGridView_WinForms, new SortableBindingList<ObjectObject_Subconscious>(ListAdapter.ObjectOtherSubconsciousViewItems), ListType.InstanceOther_Subconscious);
                                AddAllowed = true;
                                break;
                            default:
                                AddAllowed = true;
                                break;
                        }

                        DataGridView_WinForms.SelectionChanged += DataGridView_SelectionChanged;
                        CountItems = DataGridView_WinForms.RowCount;

                    }
                    else if (ListAdapter.Result_ListLoader.GUID == globals.LState_Error.GUID)
                    {
                        ListError = ListError.LoadError;
                        result = globals.LState_Error.Clone();
                    }
                }
                else if (propertyName == PropertyName.OItemList_ListAdapter_ListLoadState)
                {
                    ListLoadState = ListAdapter.ListLoadState;
                }
            }
            return result;
        }

        public AddItemAdapter GetAddFormItemType()
        {
            var result = new AddItemAdapter(globals);
            result.ListType = ListType;

            switch (ListType)
            {
                case ListType.AttributeTypes:
                    result.AddFormType = AddFormType.AttributeTypes;
                    break;
                case ListType.Classes:
                    result.AddFormType = AddFormType.Classes;
                    result.EntryItem = FilterItem_Note;
                    break;
                case ListType.RelationTypes:
                    result.AddFormType = AddFormType.RelationTypes;
                    result.ItemTypeFirst = globals.Type_RelationType;
                    break;
                case ListType.Instances:
                    result.AddFormType = AddFormType.Instances;

                    break;
                case ListType.ClassAttributeRelation:
                    result.AddFormType = AddFormType.ClassAttribute;
                    result.ItemTypeFirst = globals.Type_AttributeType;
                    break;
                case ListType.ClassClassRelation_Conscious:
                    result.AddFormType = AddFormType.ClassClass_Conscious;
                    result.ItemTypeFirst = globals.Type_Class;
                    result.ItemTypeSecond = globals.Type_RelationType;
                    break;
                case ListType.ClassClassRelation_Subconscious:
                    result.AddFormType = AddFormType.ClassClass_Subconscious;
                    result.ItemTypeFirst = globals.Type_Class;
                    result.ItemTypeSecond = globals.Type_RelationType;
                    break;
                case ListType.ClassClassRelation_Omni:

                    break;
                case ListType.ClassOtherRelation_Conscious:
                    result.AddFormType = AddFormType.ClassOther;
                    result.ItemTypeFirst = globals.Type_RelationType;
                    break;
                case ListType.InstanceAttribute_Conscious:
                    result.AddFormType = AddFormType.InstanceAttribute;
                    result.AttributeType = ListAdapter.OItem(filterItem_ObjectAtt.ID_AttributeType, globals.Type_AttributeType);
                    break;
                case ListType.InstanceInstance_Conscious:
                    result.AddFormType = AddFormType.InstanceInstance_Conscious;
                    result.ItemTypeFirst = globals.Type_Object;
                    result.EntryItem = new clsOntologyItem { GUID_Parent = filterItem_ObjectRel.ID_Parent_Other };
                    break;
                case ListType.InstanceInstance_Subconscious:
                    result.AddFormType = AddFormType.InstanceInstance_Subconscious;
                    result.ItemTypeFirst = globals.Type_Object;
                    result.EntryItem = new clsOntologyItem { GUID_Parent = filterItem_ObjectRel.ID_Parent_Object };
                    break;
                case ListType.InstanceInstance_Omni:

                    break;
                case ListType.InstanceOther_Conscious:
                    result.AddFormType = AddFormType.InstanceOther_Conscious;
                    break;
                //case ListType.InstanceOther_Subconscious:
                //    result.AddFormType = AddFormType.OntologyEditor;
                //    break;
                default:

                    break;
            }

            return result;
        }

        private clsOntologyItem AddItem(List<clsOntologyItem> selectedItems)
        {
            var result = globals.LState_Nothing.Clone();
            switch (ListType)
            {
                case ListType.AttributeTypes:
                    
                    break;
                case ListType.Classes:
                    
                    break;
                case ListType.RelationTypes:
                    
                    break;
                case ListType.Instances:
                    
                    break;
                case ListType.ClassAttributeRelation:

                    break;
                case ListType.ClassClassRelation_Conscious:

                    break;
                case ListType.ClassClassRelation_Subconscious:

                    break;
                case ListType.ClassClassRelation_Omni:

                    break;
                case ListType.ClassOtherRelation_Conscious:

                    break;
                case ListType.InstanceAttribute_Conscious:
                    
                    break;
                case ListType.InstanceInstance_Conscious:
                    
                    break;
                case ListType.InstanceInstance_Subconscious:
                    
                    break;
                case ListType.InstanceInstance_Omni:

                    break;
                case ListType.InstanceOther_Conscious:
                    
                    break;
                case ListType.InstanceOther_Subconscious:
                    
                    break;
                default:

                    break;
            }

            return result;
        }

       
        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            IdSelected = "";

            DelAllowed = false;

            if (DataGridView_WinForms.SelectedRows.Count > 0)
            {
                DelAllowed = true;
            }
            if (DataGridView_WinForms.SelectedRows.Count == 1)
            {
                selectedItem = DataGridView_WinForms.SelectedRows[0].DataBoundItem;
                switch (ListType)
                {
                    case ListType.AttributeTypes:
                        IdSelected = ((AttributeTypeViewItem)selectedItem).IdAttributeType;
                        break;
                    case ListType.Classes:
                        IdSelected = ((ClassViewItem)selectedItem).IdClass;
                        
                        break;
                    case ListType.RelationTypes:
                        IdSelected = ((RelationTypeViewItem)selectedItem).IdRelationType;
                        
                        break;
                    case ListType.Instances:
                        IdSelected = ((InstanceViewItem)selectedItem).IdInstance;

                        break;
                    case ListType.ClassAttributeRelation:
                        IdSelected = ((ClassAttributeViewItem)selectedItem).IdAttributeType;

                        break;
                    case ListType.ClassClassRelation_Conscious:
                        
                        break;
                    case ListType.ClassClassRelation_Subconscious:
                        
                        break;
                    case ListType.ClassClassRelation_Omni:
                        
                        break;
                    case ListType.ClassOtherRelation_Conscious:
                        
                        break;
                    case ListType.InstanceAttribute_Conscious:
                        IdSelected = ((ObjectAttributeViewItem)selectedItem).IdAttribute;
                        break;
                    case ListType.InstanceInstance_Conscious:
                        IdSelected = ((ObjectObject_Conscious)selectedItem).IdOther;
                        break;
                    case ListType.InstanceInstance_Subconscious:
                        IdSelected = ((ObjectObject_Subconscious)selectedItem).IdObject;
                        break;
                    case ListType.InstanceInstance_Omni:
                        
                        break;
                    case ListType.InstanceOther_Conscious:
                        IdSelected = ((ObjectObject_Conscious)selectedItem).IdOther;
                        break;
                    case ListType.InstanceOther_Subconscious:
                        IdSelected = ((ObjectObject_Subconscious)selectedItem).IdObject;
                        break;
                    default:

                        break;
                }
            }

            ConfigureMinMax();
            
        }

        public OItemListViewModelErrorType IncMin()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MinChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (classAttributeViewItem.Max == -1 || classAttributeViewItem.Min < classAttributeViewItem.Max)
                    {
                        classAttributeViewItem.Min++;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }

                    
                    

                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw == -1 || classRelationViewItem.Min_Forw < classRelationViewItem.Max_Forw)
                    {
                        classRelationViewItem.Min_Forw++;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw == -1 || classRelationViewItem.Min_Forw < classRelationViewItem.Max_Forw)
                    {
                        classRelationViewItem.Min_Forw++;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType DecMax()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MaxChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (classAttributeViewItem.Max == -1)
                    {
                        classAttributeViewItem.Max = classAttributeViewItem.Min > 0 ? classAttributeViewItem.Min : 1;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (classAttributeViewItem.Max > 1 && classAttributeViewItem.Max > classAttributeViewItem.Min)
                    {
                        classAttributeViewItem.Max--;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw == -1)
                    {
                        classRelationViewItem.Max_Forw = classRelationViewItem.Min_Forw > 0 ? classRelationViewItem.Min_Forw : 1;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (classRelationViewItem.Max_Forw > 1 && classRelationViewItem.Max_Forw > classRelationViewItem.Min_Forw)
                    {
                        classRelationViewItem.Max_Forw--;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw == -1)
                    {
                        classRelationViewItem.Max_Forw = classRelationViewItem.Min_Forw > 0 ? classRelationViewItem.Min_Forw : 1;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (classRelationViewItem.Max_Forw > 1 && classRelationViewItem.Max_Forw > classRelationViewItem.Min_Forw)
                    {
                        classRelationViewItem.Max_Forw--;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType IncMax()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MaxChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (classAttributeViewItem.Max != -1)
                    {
                        classAttributeViewItem.Max++;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw != -1)
                    {
                        classRelationViewItem.Max_Forw++;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (classRelationViewItem.Max_Forw != -1)
                    {
                        classRelationViewItem.Max_Forw++;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType SetMaxToInfinite()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MinChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    classAttributeViewItem.Max = -1;
                    var result = classAttributeViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    classRelationViewItem.Max_Forw = -1;
                    var result = classRelationViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    classRelationViewItem.Max_Forw = -1;
                    var result = classRelationViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public DeleteCounter DelClassAttributes()
        {
            List<clsClassAtt> classAttributesToDelete = new List<clsClassAtt>();

            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;

            if (dataGridView_WinForms.SelectedRows.Count > 0) 
            {
                foreach (DataGridViewRow item in dataGridView_WinForms.SelectedRows)
                {
                    if (selectedItem is ClassAttributeViewItem)
                    {
                        var classAttributeViewItem = (ClassAttributeViewItem)selectedItem;
                        classAttributesToDelete.Add(classAttributeViewItem.GetProxyItem());
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                        break;
                    }
                }

                if (!ViewModelErrorType.HasFlag(OItemListViewModelErrorType.WrongItemType))
                {
                    var result = DelClassAttributes(classAttributesToDelete);
                    return result;
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return null;
        }

        public DeleteCounter DelClassRelations()
        {
            List<clsClassRel> classRelations = new List<clsClassRel>();

            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;

            if (dataGridView_WinForms.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dataGridView_WinForms.SelectedRows)
                {
                    if (selectedItem is ClassRelationConsciousViewItem)
                    {
                        var classRelationViewItem = (ClassRelationConsciousViewItem)selectedItem;
                        classRelations.Add(classRelationViewItem.GetProxyItem());
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                        break;
                    }
                }

                if (!ViewModelErrorType.HasFlag(OItemListViewModelErrorType.WrongItemType))
                {
                    var result = DelClassRelations(classRelations);
                    return result;
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return null;
        }

        public OItemListViewModelErrorType SetMinToValue(long value)
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MinChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (value < 0)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }
                    else if (classAttributeViewItem.Max == -1 || value <= classAttributeViewItem.Max)
                    {
                        classAttributeViewItem.Min = value;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (value < 0)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }
                    else if (classRelationViewItem.Max_Forw == -1 || value <= classRelationViewItem.Max_Forw)
                    {
                        classRelationViewItem.Min_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (value < 0)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }
                    else if (classRelationViewItem.Max_Forw == -1 || value <= classRelationViewItem.Max_Forw)
                    {
                        classRelationViewItem.Min_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType SetMaxToValue(long value)
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MaxChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (value == -1)
                    {
                        classAttributeViewItem.Max = value;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (value >= classAttributeViewItem.Min && value > 0)
                    {
                        classAttributeViewItem.Max = value;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (value == -1)
                    {
                        classRelationViewItem.Max_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (value >= classRelationViewItem.Min_Forw && value > 0)
                    {
                        classRelationViewItem.Max_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (value == -1)
                    {
                        classRelationViewItem.Max_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (value >= classRelationViewItem.Min_Forw && value > 0)
                    {
                        classRelationViewItem.Max_Forw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType SetMaxBackwToValue(long value)
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MaxChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (value == -1)
                    {
                        classRelationViewItem.Max_Backw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (value >= 0)
                    {
                        classRelationViewItem.Max_Backw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (value == -1)
                    {
                        classRelationViewItem.Max_Backw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else if (value >= 0)
                    {
                        classRelationViewItem.Max_Backw = value;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MaxChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public long? GetMin()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    return classAttributeViewItem.Min;
                        
                    



                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    return classRelationViewItem.Min_Forw;





                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    return classRelationViewItem.Min_Forw;





                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                    return null;
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;
                return null;
            }

        }

        public long? GetMax()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    return classAttributeViewItem.Max;





                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationConsciousViewItem)selectedItem);

                    return classRelationItem.Max_Forw;





                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    return classRelationItem.Max_Forw;





                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                    return null;
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;
                return null;
            }

        }

        public long? GetMaxBackw()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                
                if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationConsciousViewItem)selectedItem);

                    return classRelationItem.Max_Backw;





                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    return classRelationItem.Max_Backw;





                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                    return null;
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;
                return null;
            }

        }

        public OItemListViewModelErrorType DecMin()
        {
            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;
            ViewModelErrorType &= ~OItemListViewModelErrorType.MinChangeNotValid;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    if (classAttributeViewItem.Min > 0)
                    {
                        classAttributeViewItem.Min--;
                        var result = classAttributeViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    if (classRelationViewItem.Min_Forw > 0)
                    {
                        classRelationViewItem.Min_Forw--;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    if (classRelationViewItem.Min_Forw > 0)
                    {
                        classRelationViewItem.Min_Forw--;
                        var result = classRelationViewItem.SaveItem(globals);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                        }
                    }
                    else
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.MinChangeNotValid;
                    }




                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;

                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        public OItemListViewModelErrorType SetMinTo0()
        {

            ViewModelErrorType &= ~OItemListViewModelErrorType.WrongItemType;
            ViewModelErrorType &= ~OItemListViewModelErrorType.SelectionCountError;

            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    classAttributeViewItem.Min = 0;

                    var result = classAttributeViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationConsciousViewItem)selectedItem);

                    classRelationViewItem.Min_Forw = 0;

                    var result = classRelationViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationViewItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    classRelationViewItem.Min_Forw = 0;

                    var result = classRelationViewItem.SaveItem(globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ViewModelErrorType |= OItemListViewModelErrorType.SaveError;
                    }

                }
                else
                {
                    ViewModelErrorType |= OItemListViewModelErrorType.WrongItemType;
                    
                }
            }
            else
            {
                ViewModelErrorType |= OItemListViewModelErrorType.SelectionCountError;

            }

            return ViewModelErrorType;
        }

        private void ConfigureMinMax()
        {
            if (dataGridView_WinForms.SelectedRows.Count == 1)
            {
                if (selectedItem is ClassAttributeViewItem)
                {
                    var classAttributeViewItem = ((ClassAttributeViewItem)selectedItem);

                    IsAllowedMaxDecrease = classAttributeViewItem.Max > classAttributeViewItem.Min && classAttributeViewItem.Max > 1;
                    IsAllowedMaxToInvinite = classAttributeViewItem.Max != -1;
                    IsAllowedMaxIncrease = IsAllowedMaxToInvinite;
                    IsAllowedMinIncrease = classAttributeViewItem.Max == -1 || classAttributeViewItem.Min < classAttributeViewItem.Max;
                    IsAllowedMinDecrease = classAttributeViewItem.Min > 0;
                    IsAllowedMaxEdit = IsAllowedMaxIncrease || IsAllowedMaxToInvinite || IsAllowedMaxDecrease;
                    IsAllowedMinEdit = IsAllowedMinDecrease || IsAllowedMinIncrease || IsAllowedMinTo0;
                    IsAllowedMaxBackwEdit = true;

                    var result = false;

                    if (classAttributeViewItem.Max == -1) result = true;
                    if (classAttributeViewItem.Max > classAttributeViewItem.Min && classAttributeViewItem.Max > 0) result = true;

                    IsAllowedMaxDecrease = result;
                }
                else if (selectedItem is ClassRelationConsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationConsciousViewItem)selectedItem);

                    IsAllowedMaxDecrease = classRelationItem.Max_Forw > classRelationItem.Min_Forw && classRelationItem.Max_Forw > 1;
                    IsAllowedMaxToInvinite = classRelationItem.Max_Forw != -1;
                    IsAllowedMaxIncrease = IsAllowedMaxToInvinite;
                    IsAllowedMinIncrease = classRelationItem.Max_Forw == -1 || classRelationItem.Min_Forw < classRelationItem.Max_Forw;
                    IsAllowedMinDecrease = classRelationItem.Min_Forw > 0;
                    IsAllowedMaxEdit = IsAllowedMaxIncrease || IsAllowedMaxToInvinite || IsAllowedMaxDecrease;
                    IsAllowedMinEdit = IsAllowedMinDecrease || IsAllowedMinIncrease || IsAllowedMinTo0;
                    IsAllowedMaxBackwEdit = true;

                    var result = false;

                    if (classRelationItem.Max_Forw == -1) result = true;
                    if (classRelationItem.Max_Forw > classRelationItem.Min_Forw && classRelationItem.Max_Forw > 0) result = true;

                    IsAllowedMaxDecrease = result;
                }
                else if (selectedItem is ClassRelationSubconsciousViewItem)
                {
                    var classRelationItem = ((ClassRelationSubconsciousViewItem)selectedItem);

                    IsAllowedMaxDecrease = classRelationItem.Max_Forw > classRelationItem.Min_Forw && classRelationItem.Max_Forw > 1;
                    IsAllowedMaxToInvinite = classRelationItem.Max_Forw != -1;
                    IsAllowedMaxIncrease = IsAllowedMaxToInvinite;
                    IsAllowedMinIncrease = classRelationItem.Max_Forw == -1 || classRelationItem.Min_Forw < classRelationItem.Max_Forw;
                    IsAllowedMinDecrease = classRelationItem.Min_Forw > 0;
                    IsAllowedMaxEdit = IsAllowedMaxIncrease || IsAllowedMaxToInvinite || IsAllowedMaxDecrease;
                    IsAllowedMinEdit = IsAllowedMinDecrease || IsAllowedMinIncrease || IsAllowedMinTo0;
                    IsAllowedMaxBackwEdit = true;

                    var result = false;

                    if (classRelationItem.Max_Forw == -1) result = true;
                    if (classRelationItem.Max_Forw > classRelationItem.Min_Forw && classRelationItem.Max_Forw > 0) result = true;

                    IsAllowedMaxDecrease = result;
                }
                else
                {
                    IsAllowedMaxDecrease = false;
                    IsAllowedMaxToInvinite = false;
                    IsAllowedMaxIncrease = false;
                    IsAllowedMinIncrease = false;
                    IsAllowedMinDecrease = false;
                    IsAllowedMaxEdit = false;
                    IsAllowedMinEdit = false;
                    IsAllowedMaxBackwEdit = false;

                }
            }
            else
            {
                IsAllowedMaxDecrease = false;
                IsAllowedMaxToInvinite = false;
                IsAllowedMaxIncrease = false;
                IsAllowedMinIncrease = false;
                IsAllowedMinDecrease = false;
                IsAllowedMaxEdit = false;
                IsAllowedMinEdit = false;
                IsAllowedMaxBackwEdit = false;

            }

        }

        private void ListAdapter_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var result = FillDataControl(e.PropertyName);
            RaisePropertyChanged(e.PropertyName);

        }
    }
}
