﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ObjectRelationViewItem : NotifyPropertyChange
    {
        internal clsObjectRel proxyItem;

        public string IdObject
        {
            get { return proxyItem.ID_Object; }
            set
            {
                proxyItem.ID_Object = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_ID_Object);
            }
        }

        public string IdOther
        {
            get { return proxyItem.ID_Other; }
            set
            {
                proxyItem.ID_Other = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_ID_Other);
            }
        }

        public string IdParentObject
        {
            get { return proxyItem.ID_Parent_Object; }
            set
            {
                proxyItem.ID_Parent_Object = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_ID_Parent_Object);
            }
        }

        public string IdParentOther
        {
            get { return proxyItem.ID_Parent_Other; }
            set
            {
                proxyItem.ID_Parent_Other = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_ID_Parent_Other);
            }
        }

        public string IdRelationType
        {
            get { return proxyItem.ID_RelationType; }
            set
            {
                proxyItem.ID_RelationType = value;
                RaisePropertyChanged(PropertyName.OItemList_ObjectRel_ID_RelationType);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveObjRel(new List<clsObjectRel> { proxyItem });

            return result;
        }

        public clsObjectRel GetProxyItem()
        {
            return proxyItem;
        }


        public ObjectRelationViewItem(clsObjectRel proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
