﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class ClassRelationConsciousViewItem : ClassRelationViewItem
    {
        
        public string NameClassLeft
        {
            get { return proxyItem.Name_Class_Left; }
            set
            {
                proxyItem.Name_Class_Left = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_NameClassLeft);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 1)]
        public string NameClassRight
        {
            get { return proxyItem.Name_Class_Right; }
            set
            {
                proxyItem.Name_Class_Right = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_NameClassRight);
            }
        }

        [DataViewColumn(IsVisible =true,
            DisplayOrder = 0)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_Class_Right = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_NameRelationType);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 2)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_Ontology);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 3)]
        public long Min_Forw
        {
            get { return proxyItem.Min_Forw ?? 0; }
            set
            {
                proxyItem.Min_Forw = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_Min_Forw);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 4)]
        public long Max_Forw
        {
            get { return proxyItem.Max_Forw ?? 0; }
            set
            {
                proxyItem.Max_Forw = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_Max_Forw);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 5)]
        public long Max_Backw
        {
            get { return proxyItem.Max_Backw ?? 0; }
            set
            {
                proxyItem.Max_Backw = value;
                RaisePropertyChanged(PropertyName.OItemList_ClassRel_Max_Backw);
            }
        }

        public clsClassRel GetProxyItem()
        {
            return proxyItem;
        }

        public ClassRelationConsciousViewItem(clsClassRel proxyItem) : base(proxyItem)
        {
            
        }
    }
}
