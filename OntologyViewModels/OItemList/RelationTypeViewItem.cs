﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.Attributes;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.OItemList
{
    public class RelationTypeViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        public string IdRelationType
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(PropertyName.OItemList_RelationType_IdRelationType);
            }
        }


        [DataViewColumn(IsVisible = true, DisplayOrder = 0)]
        public string NameRelationType
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(PropertyName.OItemList_RelationType_NameRelationType);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveRelationTypes(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        public RelationTypeViewItem(clsOntologyItem proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
