﻿using OntologyViewModels.Attributes;
using OntologyViewModels.OItemList;
using Structure_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OntologyViewModels.DataGridViewWinForms
{
    public class DataGridViewAdapter<TType>
    {

        private delegate void ItemListChanged();
        private DataGridView dataGridView;
        public DataGridView DataGridView
        {
            get { return dataGridView; }
            set
            {
                dataGridView = value;
            }
        }

        private SortableBindingList<TType> listViewItems;
        public SortableBindingList<TType> ListViewItems
        {
            get { return listViewItems; }
            set
            {
                listViewItems = value;
            }
        }

        private ListType listType;
        public ListType ListType
        {
            get { return listType; }
            set
            {
                listType = value;
            }
        }

        public DataGridViewAdapter(DataGridView dataGridView, SortableBindingList<TType> listViewItems, ListType listType)
        {
            DataGridView = dataGridView;
            this.ListViewItems = listViewItems;
            ListType = listType;

            BindDataGridView();
            ConfigureColumns();
        }

        private void BindDataGridView()
        {
            if (DataGridView.InvokeRequired)
            {
                var itemListChanged = new ItemListChanged(BindDataGridView);
                DataGridView.Invoke(itemListChanged);
            }
            else
            {
                DataGridView.DataSource = ListViewItems;
            }
            
        }

        private void ConfigureColumns()
        {
            if (DataGridView.InvokeRequired)
            {
                var itemListChanged = new ItemListChanged(ConfigureColumns);
                DataGridView.Invoke(itemListChanged);
            }
            else
            {
                var columns = DataGridView.Columns.Cast<DataGridViewColumn>().ToList();
                var ttypeProperties = typeof(TType).GetProperties().Cast<PropertyInfo>().ToList();
                var columnProperties = ttypeProperties.Select(prop =>
                {
                    var columnAttribute = new { Property = prop, ColumnAttribute = prop.GetCustomAttribute<DataViewColumnAttribute>() };
                    return columnAttribute;
                }).ToList();

                var invisbleColumns = (from colProp in columnProperties.Where(prop => prop.ColumnAttribute == null)
                                       join col in columns on colProp.Property.Name equals col.DataPropertyName
                                       select new { Column = col, PropertyItem = colProp }).ToList();
                var visibleColumns = (from colProp in columnProperties.Where(prop => prop.ColumnAttribute != null && prop.ColumnAttribute.IsVisible)
                                      join col in columns on colProp.Property.Name equals col.DataPropertyName
                                      select new { Column = col, PropertyItem = colProp }).OrderBy(colProp => colProp.PropertyItem.ColumnAttribute.DisplayOrder).ToList();

                invisbleColumns.ForEach(colAndProp =>
                {
                    colAndProp.Column.Visible = false;
                });

                var orderId = 0;
                visibleColumns.ForEach(colAndProp =>
                {
                    colAndProp.Column.DisplayIndex = orderId;
                    orderId++;
                });
            }
            


        }

        
    }
}
