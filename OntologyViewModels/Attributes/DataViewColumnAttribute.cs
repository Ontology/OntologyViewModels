﻿using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.Attributes
{
    public class DataViewColumnAttribute:Attribute
    {
        public bool IsVisible { get; set; }
        public bool IsReadonly { get; set; }
        public int DisplayOrder { get; set; }
        public bool Caption { get; set; }

    }
}
