﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyViewModels.DataAdapter
{
    [Flags]
    public enum AddFormType
    {
        AttributeTypes = 0,
        RelationTypes = 1,
        Classes = 2,
        Instances = 4,
        ClassAttribute = 8,
        ClassClass_Conscious = 16,
        ClassClass_Subconscious = 32,
        ClassOther = 64,
        InstanceAttribute = 128,
        InstanceInstance_Conscious = 256,
        InstanceInstance_Subconscious = 512,
        InstanceOther_Conscious = 1024,
    }
    [Flags]
    public enum NameAddError
    {
        NotChecked = 0,
        None = 1,
        Error = 2,
        Exists = 4
    }
    public enum NonNameErrorType
    {
        None = 0,
        Type1IsWrong = 1,
        Type2IsWrong = 2,
        Count1 = 4
    }
    public class AddItemAdapter
    {
        private Globals globals;
        private OntologyModDBConnector dbConnectorClassAttributes;
        private OntologyModDBConnector dbConnectorExists;
        private OntologyModDBConnector dbConnectorSave;

        public clsOntologyItem AttributeType { get; set; }
        public string ItemTypeFirst { get; set; }
        public string ItemTypeSecond { get; set; }
        public clsOntologyItem EntryItem { get; set; }
        public ListType ListType { get; set; }

        public NonNameErrorType NonNameErrorType { get; set; }

        private AddFormType addFormType;
        public AddFormType AddFormType
        {
            get { return addFormType; }
            set
            {
                addFormType = value;
                switch (addFormType)
                {
                    case AddFormType.ClassAttribute:
                        AddCount1 = 1;
                        break;
                    case AddFormType.InstanceAttribute:
                        AddCount1 = -1;
                        break;
                    case AddFormType.ClassClass_Conscious:
                    case AddFormType.ClassClass_Subconscious:
                        AddCount1 = 1;
                        AddCount2 = 1;
                        break;
                    case AddFormType.ClassOther:
                        AddCount1 = 1;
                        break;
                    
                    case AddFormType.AttributeTypes:
                    case AddFormType.Classes:
                    case AddFormType.RelationTypes:
                    case AddFormType.Instances:
                        AddCount1 = -1;
                        break;
                    case AddFormType.InstanceInstance_Conscious:
                    case AddFormType.InstanceInstance_Subconscious:
                    case AddFormType.InstanceOther_Conscious:
                        AddCount1 = -1;
                        break;

                }
            }
        }
        public int AddCount1 { get; set; }
        public int AddCount2 { get; set; }

        public List<clsOntologyItem> ResultItems_OntologyEditor1 { get; set; }
        public List<clsOntologyItem> ResultItems_OntologyEditor2 { get; set; }
        
        public List<ResultAttribute> ResultAttributes { get; set; }
        public string TypeApplied1 { get; set; }
        public string TypeApplied2 { get; set; }
        public List<string> ExistingNames { get; set; }

        public NameAddError NamesError { get; set; }
        
        public bool IsOk
        {
            get
            {
                return IsOk1 && IsOk2;
            }
        }

        private bool? isOk1 = null;
        public bool IsOk1
        {
            get
            {
                if (isOk1 == null)
                {
                    isOk1 = CheckOk(true);
                }
                return isOk1.Value;
            }
        }

        private bool? isOk2 = null;
        public bool IsOk2
        {
            get
            {
                if (isOk2 == null && (
                    AddFormType == AddFormType.ClassClass_Conscious ||
                    AddFormType == AddFormType.ClassClass_Subconscious))
                {
                    isOk2 = CheckOk(false);
                }
                else
                {
                    isOk2 = true;
                }
                return isOk2.Value;
            }
        }

        public AddItemAdapter(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }


        private void Initialize()
        {
            dbConnectorClassAttributes = new OntologyModDBConnector(globals);
            dbConnectorExists = new OntologyModDBConnector(globals);
            dbConnectorSave = new OntologyModDBConnector(globals);
            AddCount1 = -1;
            AddCount2 = -1;
        }

        private clsOntologyItem ExistNames()
        {
            if (AddFormType == AddFormType.AttributeTypes ||
                AddFormType == AddFormType.Classes ||
                AddFormType == AddFormType.RelationTypes ||
                AddFormType == AddFormType.Instances)
            {
                var resultString = ResultAttributes != null ? ResultAttributes.Where(attribut => attribut.ResultStr != null).ToList() : new List<ResultAttribute>();
                if (resultString.Any())
                {
                    

                    clsOntologyItem result = null;
                    switch (AddFormType)
                    {
                        case AddFormType.AttributeTypes:
                            var searchAttributeTypes = resultString.Select(str => new clsOntologyItem
                            {
                                Name = str.ResultStr,
                                Type = globals.Type_AttributeType
                            }).ToList();
                            result = dbConnectorExists.GetDataAttributeType(searchAttributeTypes);
                            ExistingNames = (from searchItem in resultString
                                             from foundItem in dbConnectorExists.AttributeTypes
                                             where searchItem.ResultStr.ToLower() == foundItem.Name.ToLower()
                                             select searchItem.ResultStr).ToList();

                            break;
                        case AddFormType.Classes:
                            var searchClasses = resultString.Select(str => new clsOntologyItem
                            {
                                Name = str.ResultStr,
                                Type = globals.Type_Class
                            }).ToList();
                            result = dbConnectorExists.GetDataClasses(searchClasses);
                            ExistingNames = (from searchItem in resultString
                                             from foundItem in dbConnectorExists.Classes1
                                             where searchItem.ResultStr.ToLower() == foundItem.Name.ToLower()
                                             select searchItem.ResultStr).ToList();
                            break;
                        case AddFormType.RelationTypes:
                            var searchRelationTypes = resultString.Select(str => new clsOntologyItem
                            {
                                Name = str.ResultStr,
                                Type = globals.Type_RelationType
                            }).ToList();
                            result = dbConnectorExists.GetDataRelationTypes(searchRelationTypes);
                            ExistingNames = (from searchItem in resultString
                                             from foundItem in dbConnectorExists.RelationTypes
                                             where searchItem.ResultStr.ToLower() == foundItem.Name.ToLower()
                                             select searchItem.ResultStr).ToList();
                            break;
                        case AddFormType.Instances:
                            var searchInstances = resultString.Select(str => new clsOntologyItem
                            {
                                Name = str.ResultStr,
                                GUID_Parent = EntryItem.Type == globals.Type_Object ? EntryItem.GUID_Parent : EntryItem.GUID,
                                Type = globals.Type_RelationType
                            }).ToList();
                            result = dbConnectorExists.GetDataObjects(searchInstances);
                            ExistingNames = (from searchItem in resultString
                                             from foundItem in dbConnectorExists.Objects1
                                             where searchItem.ResultStr.ToLower() == foundItem.Name.ToLower()
                                             select searchItem.ResultStr).ToList();
                            break;
                    }
                    

                    if (result == null)
                    {
                        return globals.LState_Error.Clone();
                    }

                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        
                        if (ExistingNames.Any())
                        {

                            return globals.LState_Exists.Clone();

                        }
                        else
                        {
                            return globals.LState_Insert.Clone();
                        }
                    }
                    
                    return result;
                }
                else
                {
                    return globals.LState_Error.Clone();
                }

            }
            else
            {
                return globals.LState_Error.Clone();
            }
            
        }

        public clsOntologyItem Add_AtirbuteTypes()
        {
            if (IsOk)
            {
                var attributeTypesToAdd = ResultAttributes.Select(str => new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = str.ResultStr,
                    GUID_Parent = AttributeType.GUID,
                    Type = globals.Type_AttributeType
                    
                }).ToList();

                var result = dbConnectorSave.SaveAttributeTypes(attributeTypesToAdd);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_RelationTypes()
        {
            if (IsOk)
            {
               var relationTypes = ResultAttributes.Select(str => new clsOntologyItem
               {
                   GUID = globals.NewGUID,
                   Name = str.ResultStr,
                   Type = globals.Type_RelationType

               }).ToList();

                var result = dbConnectorSave.SaveRelationTypes(relationTypes);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_Classes(clsOntologyItem oItemClass)
        {
            if (oItemClass != null && oItemClass.Type == globals.Type_Class && IsOk)
            {
                var classes = ResultAttributes.Select(str => new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = str.ResultStr,
                    GUID_Parent = oItemClass.GUID,
                    Type = globals.Type_Class

                }).ToList();

                var result = dbConnectorSave.SaveClass(classes);

                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_Instances(clsOntologyItem oItemClass)
        {
            if (oItemClass != null && oItemClass.Type == globals.Type_Class && (IsOk || NamesError == NameAddError.Exists))
            {
                var instances = ResultAttributes.Select(str => new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = str.ResultStr,
                    GUID_Parent = oItemClass.GUID,
                    Type = globals.Type_Object

                }).ToList();

                var result = dbConnectorSave.SaveObjects(instances);

                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_ClassAttributes(clsOntologyItem oItemClass)
        {
            if (IsOk && oItemClass != null && oItemClass.Type == globals.Type_Class)
            {
                var classAttributes = ResultItems_OntologyEditor1.Select(attrib => new clsClassAtt
                {
                    ID_AttributeType = attrib.GUID,
                    ID_DataType = attrib.GUID_Parent,
                    ID_Class = oItemClass.GUID,
                    Min =  1,
                    Max = -1
                }).ToList();

                var result = dbConnectorSave.SaveClassAtt(classAttributes);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_ClassClass_Conscious(clsOntologyItem oItemClass)
        {
            if (IsOk && oItemClass != null && oItemClass.Type == globals.Type_Class)
            {
                var classClass = ResultItems_OntologyEditor1.Select(cls => new clsClassRel
                {
                    ID_Class_Left = oItemClass.GUID,
                    ID_RelationType = ResultItems_OntologyEditor2.First().GUID,
                    ID_Class_Right = cls.GUID,
                    Ontology = globals.Type_Class,
                    Min_Forw = 1,
                    Max_Forw = -1,
                    Max_Backw = -1
                }).ToList();

                var result = dbConnectorSave.SaveClassRel(classClass);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_ClassClass_Subconscious(clsOntologyItem oItemClass)
        {
            if (IsOk && oItemClass != null && oItemClass.Type == globals.Type_Class)
            {
                var classClass = ResultItems_OntologyEditor1.Select(cls => new clsClassRel
                {
                    ID_Class_Left = cls.GUID,
                    ID_RelationType = ResultItems_OntologyEditor2.First().GUID,
                    ID_Class_Right = oItemClass.GUID,
                    Ontology = globals.Type_Class,
                    Min_Forw = 1,
                    Max_Forw = -1,
                    Max_Backw = -1
                }).ToList();

                var result = dbConnectorSave.SaveClassRel(classClass);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem Add_ClassOther(clsOntologyItem oItemClass)
        {
            if (IsOk)
            {
                var classOther = ResultItems_OntologyEditor1.Select(item => new clsClassRel
                {
                    ID_Class_Left = oItemClass.GUID,
                    ID_RelationType = ResultItems_OntologyEditor1.First().GUID,
                    ID_Class_Right = item.Type == globals.Type_Class ? item.GUID : null,
                    Ontology = item.Type == globals.Type_Class ? item.Type : globals.Type_Other,
                    Min_Forw = 1,
                    Max_Forw = -1,
                    Max_Backw = -1
                }).ToList();

                var result = dbConnectorSave.SaveClassRel(classOther);
                return result;
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        private clsOntologyItem Add_InstanceAttribute()
        {
            if (IsOk)
            {
                List<clsObjectAtt> objectAttributes = null;
                if (AttributeType.GUID_Parent == globals.DType_Bool.GUID)
                {
                    objectAttributes = ResultAttributes.Select(value => new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = AttributeType.GUID,
                        ID_Class = EntryItem.GUID_Parent,
                        ID_DataType = AttributeType.GUID_Parent,
                        ID_Object = EntryItem.GUID,
                        OrderID =  value.OrderId,
                        Val_Name = value.ResultBool.ToString(),
                        Val_Bool = value.ResultBool
                        
                    }).ToList();
                }
                else if (AttributeType.GUID_Parent == globals.DType_DateTime.GUID)
                {
                    objectAttributes = ResultAttributes.Select(value => new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = AttributeType.GUID,
                        ID_Class = EntryItem.GUID_Parent,
                        ID_DataType = AttributeType.GUID_Parent,
                        ID_Object = EntryItem.GUID,
                        OrderID = value.OrderId,
                        Val_Name = value.ResultDateTime.ToString(),
                        Val_Date = value.ResultDateTime

                    }).ToList();
                }
                else if (AttributeType.GUID_Parent == globals.DType_Int.GUID)
                {
                    objectAttributes = ResultAttributes.Select(value => new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = AttributeType.GUID,
                        ID_Class = EntryItem.GUID_Parent,
                        ID_DataType = AttributeType.GUID_Parent,
                        ID_Object = EntryItem.GUID,
                        OrderID = value.OrderId,
                        Val_Name = value.ResultInt.ToString(),
                        Val_Int = value.ResultInt

                    }).ToList();

                }
                else if (AttributeType.GUID_Parent == globals.DType_Real.GUID)
                {
                    objectAttributes = ResultAttributes.Select(value => new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = AttributeType.GUID,
                        ID_Class = EntryItem.GUID_Parent,
                        ID_DataType = AttributeType.GUID_Parent,
                        ID_Object = EntryItem.GUID,
                        OrderID = value.OrderId,
                        Val_Name = value.ResultDouble.ToString(),
                        Val_Double = value.ResultDouble

                    }).ToList();
                }
                else if (AttributeType.GUID_Parent == globals.DType_String.GUID)
                {
                    objectAttributes = ResultAttributes.Select(value => new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = AttributeType.GUID,
                        ID_Class = EntryItem.GUID_Parent,
                        ID_DataType = AttributeType.GUID_Parent,
                        ID_Object = EntryItem.GUID,
                        OrderID = value.OrderId,
                        Val_Name = value.ResultStr.Length > 255 ? value.ResultStr.Substring(0,254) : value.ResultStr,
                        Val_String = value.ResultStr

                    }).ToList();
                }
                else
                {
                    return globals.LState_Error.Clone();
                }

                if (objectAttributes != null && objectAttributes.Any())
                {
                    var result = dbConnectorSave.SaveObjAtt(objectAttributes);
                    return result;
                }
                else
                {
                    return globals.LState_Error.Clone();
                }
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        private clsOntologyItem Add_InstanceInstance_Conscious()
        {
            if (IsOk)
            {

                return globals.LState_Success.Clone();
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        private clsOntologyItem Add_InstanceInstance_Subconscious()
        {
            if (IsOk)
            {

                return globals.LState_Success.Clone();
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        private clsOntologyItem Add_InstanceOther_Conscious()
        {
            if (IsOk)
            {

                return globals.LState_Success.Clone();
            }
            else
            {
                return globals.LState_Error.Clone();
            }
        }

        public bool CheckOk(bool firstFormResult = true)
        {
            
            switch (AddFormType)
            {
                case AddFormType.InstanceAttribute:
                    if (AttributeType != null)
                    {
                        if (AttributeType.GUID_Parent == globals.DType_Bool.GUID)
                        {
                            isOk1 = ResultAttributes != null ? ResultAttributes.Any(resultVal => resultVal.ResultBool != null) : false;
                            return IsOk;
                        }
                        else if (AttributeType.GUID_Parent == globals.DType_DateTime.GUID)
                        {
                            isOk1 = ResultAttributes != null ? ResultAttributes.Any(resultVal => resultVal.ResultDateTime != null) : false;
                            return IsOk;
                        }
                        else if (AttributeType.GUID_Parent == globals.DType_Int.GUID)
                        {
                            isOk1 = ResultAttributes != null ? ResultAttributes.Any(resultVal => resultVal.ResultInt != null) : false;
                            return IsOk;
                        }
                        else if (AttributeType.GUID_Parent == globals.DType_Real.GUID)
                        {
                            isOk1 = ResultAttributes != null ? ResultAttributes.Any(resultVal => resultVal.ResultDouble != null) : false;
                            return IsOk;
                        }
                        else if (AttributeType.GUID_Parent == globals.DType_String.GUID)
                        {
                            isOk1 = ResultAttributes != null ? ResultAttributes.Any(resultVal => resultVal.ResultStr != null) : false;
                            return IsOk;
                        }
                    }
                    
                    break;
                case AddFormType.ClassAttribute:
                    isOk1 = true;

                    NonNameErrorType = NonNameErrorType.None;

                    if (TypeApplied1 != globals.Type_AttributeType)
                    {
                        NonNameErrorType = NonNameErrorType.Type1IsWrong;
                        isOk1 = false;

                        return IsOk1;
                    }

                    if (ResultItems_OntologyEditor1.Count != AddCount1)
                    {
                        NonNameErrorType = NonNameErrorType.Count1;
                        isOk1 = false;

                        return IsOk1;
                    }
                    
                    return IsOk1;
                    
                case AddFormType.ClassClass_Conscious:
                case AddFormType.ClassClass_Subconscious:
                    if (firstFormResult)
                    {
                        if (TypeApplied1 == globals.Type_Class)
                        {
                            isOk1 = ResultItems_OntologyEditor1 != null && ResultItems_OntologyEditor1.Count == AddCount1;
                            return IsOk1;
                        }
                    }
                    else
                    {
                        if (TypeApplied2 == globals.Type_RelationType)
                        {
                            isOk2 = ResultItems_OntologyEditor2 != null && ResultItems_OntologyEditor1.Count == AddCount2;
                            return IsOk2;
                        }
                    }
                    
                    break;
                
                case AddFormType.ClassOther:
                    if (TypeApplied1 == globals.Type_RelationType)
                    {
                        isOk1 = ResultItems_OntologyEditor1 != null && ResultItems_OntologyEditor1.Count == AddCount1;
                        return IsOk1;
                    }

                    break;
                case AddFormType.AttributeTypes:
                    var result = IsNameOk();
                    if (firstFormResult)
                    {
                        isOk1 = result;
                        return IsOk1;
                    }
                    else
                    {
                        isOk2 = AttributeType != null;
                        return IsOk2;
                    }
                   
                case AddFormType.RelationTypes:
                    isOk1 = IsNameOk();
                    return IsOk1;

                case AddFormType.Instances:
                    if (EntryItem.Type == globals.Type_Object || EntryItem.Type == globals.Type_Class)
                    {
                        isOk1 = IsNameOk();
                    }
                    else
                    {
                        isOk1 = false;
                        
                    }
                    return IsOk1;

                case AddFormType.Classes:
                    if (EntryItem.Type == globals.Type_Class)
                    {
                        isOk1 = IsNameOk();
                    }
                    else
                    {
                        isOk1 = false;
                    }
                        

                    return IsOk1;
                case AddFormType.InstanceInstance_Conscious:
                case AddFormType.InstanceInstance_Subconscious:
                    if (TypeApplied1 == ItemTypeFirst)
                    {
                        if (ResultItems_OntologyEditor1 != null && AddCount1 == -1 ? ResultItems_OntologyEditor1.Any() : ResultItems_OntologyEditor1.Count == AddCount1)
                        {
                            if (EntryItem != null)
                            {
                                ResultItems_OntologyEditor1 = ResultItems_OntologyEditor1.Where(itms => itms.GUID_Parent == EntryItem.GUID_Parent).ToList();
                                isOk1 = ResultItems_OntologyEditor1.Any();
                            }
                            else
                            {
                                isOk1 = false;
                            }

                            return IsOk1;
                            
                        }
                        
                    }
                    break;

                case AddFormType.InstanceOther_Conscious:
                    if (ResultItems_OntologyEditor1 != null && AddCount1 == -1 ? ResultItems_OntologyEditor1.Any() : ResultItems_OntologyEditor1.Count == AddCount1)
                    {
                        if (EntryItem != null)
                        {
                            ResultItems_OntologyEditor1 = ResultItems_OntologyEditor1.Where(itms => itms.GUID_Parent == EntryItem.GUID_Parent).ToList();
                            isOk1 = ResultItems_OntologyEditor1.Any();
                            return IsOk1;
                        }

                        isOk1 = true;
                        return IsOk1;

                    }
                    break;
            }

            if (firstFormResult)
            {
                isOk1 = false;
                return IsOk1;
            }
            else
            {
                isOk2 = false;
                return IsOk2;
            }
            
            
        }

        private bool IsNameOk()
        {
            var resultStr = ResultAttributes != null ? ResultAttributes.Select(resultAttr => resultAttr.ResultStr).ToList() : new List<string>();
            var result = resultStr != null && AddCount1 == -1 ? resultStr.Any() : resultStr.Count == AddCount1;
            if (result)
            {
                var resultOItem = ExistNames();
                if (resultOItem.GUID == globals.LState_Insert.GUID)
                {
                    NamesError = NameAddError.None;
                    result = true;
                }
                else if (resultOItem.GUID == globals.LState_Exists.GUID)
                {
                    NamesError = NameAddError.Exists;
                    result = false;

                }
                else
                {
                    NamesError = NameAddError.Error;
                    result = false;

                }

            }

            return result;
        }

    }
}
