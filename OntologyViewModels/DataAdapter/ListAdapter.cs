﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologyViewModels.DataAdapter
{
    [Flags]
    public enum ListLoadStateItem
    {

        None = 0,
        ListLoadPending = 1,
        ListLoadLoaded = 2,
        AdvancedFilterPending = 4,
        AdvancedFilterLoaded = 8
    }
    public class ListAdapter : NotifyPropertyChange
    {
        private Globals globals;
        private OntologyModDBConnector dbReadConnector_AttributeTypes;
        private OntologyModDBConnector dbReadConnector_Classes;
        private OntologyModDBConnector dbReadConnector_RelationType;
        private OntologyModDBConnector dbReadConnector_Instance;
        private OntologyModDBConnector dbReadConnector_ClassAttributes;
        private OntologyModDBConnector dbReadConnector_ClassRelations;
        private OntologyModDBConnector dbReadConnector_ObjectAttributes;
        private OntologyModDBConnector dbReadConnector_ObjectRelations;
        private OntologyModDBConnector dbReadConnector_AdvancedFilter_Conscious;
        private OntologyModDBConnector dbReadConnector_AdvancedFilter_Subconscious;
        private OntologyModDBConnector dbReadConnector_OItem;
        private OntologyModDBConnector dbWriteConnector;

        private Thread threadListLoader;

        private ListLoadStateItem listLoadState;
        public ListLoadStateItem ListLoadState
        {
            get { return listLoadState; }
            set
            {
                listLoadState = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ListLoadState);
            }
        }

        private clsOntologyItem filterItemNote;
        public clsOntologyItem FilterItemNote
        {
            get { return filterItemNote; }
            set
            {
                filterItemNote = value;

            }
        }

        private clsClassAtt filterItemClassAtt;
        public clsClassAtt FilterItemClassAtt
        {
            get { return filterItemClassAtt; }
            set
            {
                filterItemClassAtt = value;
            }
        }

        private clsClassRel filterItemClassRel;
        public clsClassRel FilterItemClassRel
        {
            get { return filterItemClassRel; }
            set
            {
                filterItemClassRel = value;
            }
        }

        private clsObjectAtt filterItemObjectAttribute;
        public clsObjectAtt FilterItemObjectAttribute
        {
            get { return filterItemObjectAttribute; }
            set
            {
                filterItemObjectAttribute = value;
            }
        }

        private clsObjectRel filterItemObjectRelation;
        public clsObjectRel FilterItemObjectRelation
        {
            get { return filterItemObjectRelation; }
            set
            {
                filterItemObjectRelation = value;
            }
        }

        private ListType listType;

        private bool exact;
        public bool Exact
        {
            get { return exact; }
            set
            {
                exact = value;
            }
        }

        private List<ClassViewItem> classViewItems;
        public List<ClassViewItem> ClassViewItems
        {
            get { return classViewItems; }
            set
            {
                classViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ClassViewItems);
            }
        }

        private List<AttributeTypeViewItem> attributeTypeViewItems;
        public List<AttributeTypeViewItem> AttributeTypeViewItems
        {
            get { return attributeTypeViewItems; }
            set
            {
                attributeTypeViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_AttributeTypeViewItems);
            }
        }

        private List<RelationTypeViewItem> relationTypeViewItems;
        public List<RelationTypeViewItem> RelationTypeViewItems
        {
            get { return relationTypeViewItems; }
            set
            {
                relationTypeViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_RelationTypeViewItems);
            }
        }

        private List<InstanceViewItem> instanceViewItems;
        public List<InstanceViewItem> InstanceViewItems
        {
            get { return instanceViewItems; }
            set
            {
                instanceViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_InstanceViewItems);
            }
        }

        private List<ClassAttributeViewItem> classAttViewItems;
        public List<ClassAttributeViewItem> ClassAttViewItems
        {
            get { return classAttViewItems; }
            set
            {
                classAttViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ClassAttViewItems);
            }
        }

        private List<ClassRelationConsciousViewItem> classRelConciousViewItems;
        public List<ClassRelationConsciousViewItem> ClassRelConciousViewItems
        {
            get { return classRelConciousViewItems; }
            set
            {
                classRelConciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ClassRelConsciousViewItems);
            }
        }

        private List<ClassRelationSubconsciousViewItem> classRelSubconsciousViewItems;
        public List<ClassRelationSubconsciousViewItem> ClassRelSubconsciousViewItems
        {
            get { return classRelSubconsciousViewItems; }
            set
            {
                classRelSubconsciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ClassRelSubconsciousViewItems);
            }
        }

        private List<ClassRelationOmniViewItem> classRelOmniViewItems;
        public List<ClassRelationOmniViewItem> ClassRelOmniViewItems
        {
            get { return classRelOmniViewItems; }
            set
            {
                classRelOmniViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ClassRelOmniViewItems);
            }
        }

        private List<ObjectAttributeViewItem> objectAttributeViewItems;
        public List<ObjectAttributeViewItem> ObjectAttributeViewItems
        {
            get { return objectAttributeViewItems; }
            set
            {
                objectAttributeViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectAttributeViewItems);
            }
        }

        private List<ObjectObject_Conscious> objectObjectConsciousViewItems;
        public List<ObjectObject_Conscious> ObjectObjectConsciousViewItems
        {
            get { return objectObjectConsciousViewItems; }
            set
            {
                objectObjectConsciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectObjectConsciousViewItems);
            }
        }

        private List<ObjectObject_Subconscious> objectObjectSubconsciousViewItems;
        public List<ObjectObject_Subconscious> ObjectObjectSubconsciousViewItems
        {
            get { return objectObjectSubconsciousViewItems; }
            set
            {
                objectObjectSubconsciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectObjectSubconsciousViewItems);
            }
        }

        private List<ObjectObject_Conscious> objectOtherConsciousViewItems;
        public List<ObjectObject_Conscious> ObjectOtherConsciousViewItems
        {
            get { return objectOtherConsciousViewItems; }
            set
            {
                objectOtherConsciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectOtherConsciousViewItems);
            }
        }

        private List<ObjectObject_Subconscious> objectOtherSubconsciousViewItems;
        public List<ObjectObject_Subconscious> ObjectOtherSubconsciousViewItems
        {
            get { return objectOtherSubconsciousViewItems; }
            set
            {
                objectOtherSubconsciousViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectOtherSubconsciousViewItems);
            }
        }

        private List<ObjectObject_Omni> objectObjectOmniViewItems;
        public List<ObjectObject_Omni> ObjectObjectOmniViewItems
        {
            get { return objectObjectOmniViewItems; }
            set
            {
                objectObjectOmniViewItems = value;
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_ObjectObjectOmniViewItems);
            }
        }

        private clsOntologyItem result_ListLoader;
        public clsOntologyItem Result_ListLoader
        {
            get { return result_ListLoader; }
            set
            {
                result_ListLoader = value;
                if (result_ListLoader.GUID != globals.LState_Nothing.GUID)
                {
                    ListLoadStateItem listLoadStatePre = ListLoadState;
                    if (ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterPending))
                    {
                        listLoadStatePre &= ~ListLoadStateItem.AdvancedFilterPending;
                        listLoadStatePre |= ListLoadStateItem.AdvancedFilterLoaded;
                    }

                    if (ListLoadState.HasFlag(ListLoadStateItem.ListLoadPending))
                    {
                        listLoadStatePre &= ~ListLoadStateItem.ListLoadPending;
                        listLoadStatePre |= ListLoadStateItem.ListLoadLoaded;
                    }

                    ListLoadState = listLoadStatePre;
                }
                RaisePropertyChanged(PropertyName.OItemList_ListAdapter_Result_ListLoader);
            }
        }

        public DeleteCounter DelClassAttributes(List<clsClassAtt> classAttributes)
        {
            var counter = new DeleteCounter
            {
                CountToDo = classAttributes.Count,
                CountDone = 0,
                CountError = 0,
                CountRelated = 0
            };
            
            classAttributes.ForEach(classAtt =>
            {
                var result = dbWriteConnector.DelClassAttType(new clsOntologyItem { GUID = classAtt.ID_Class, Type = globals.Type_Class }, new clsOntologyItem { GUID = classAtt.ID_AttributeType, Type = globals.Type_AttributeType });
                if (result.GUID == globals.LState_Success.GUID)
                {
                    counter.CountDone++;
                }
                else if (result.GUID == globals.LState_Relation.GUID)
                {
                    counter.CountRelated++;
                }
                else if (result.GUID == globals.LState_Error.GUID)
                {
                    counter.CountError++;
                }
            });

            return counter;
        }

        public DeleteCounter DelClassRelations(List<clsClassRel> classRelations)
        {
            var counter = new DeleteCounter
            {
                CountToDo = classRelations.Count,
                CountDone = 0,
                CountError = 0,
                CountRelated = 0
            };

            var result = dbWriteConnector.DelClassRel(classRelations);
            if (result.GUID == globals.LState_Success.GUID)
            {
                counter.CountDone = classRelations.Count;
            }
            else if (result.GUID == globals.LState_Relation.GUID)
            {
                counter.CountRelated = classRelations.Count;
            }
            else if (result.GUID == globals.LState_Error.GUID)
            {
                counter.CountError = classRelations.Count;
            }
            return counter;
        }

        public clsOntologyItem OItem(string guid, string type)
        {
            return dbReadConnector_OItem.GetOItem(guid, type);
        }

        public ListAdapter(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }

        public clsOntologyItem SaveClassAttributeList(List<clsClassAtt> classAttributes)
        {
            return dbWriteConnector.SaveClassAtt(classAttributes);
        }

        public void Initialize_AdvancedFilter(ListType listType,
            clsOntologyItem advancedFilter_Class = null,
            clsOntologyItem advancedFilter_Object = null,
            clsOntologyItem advancedFilter_RelationType = null,
            clsOntologyItem advancedFilter_Direction = null)
        {
            ListLoadState |= ListLoadStateItem.AdvancedFilterPending;
            Result_ListLoader = globals.LState_Nothing.Clone();

            switch (listType)
            {
                case ListType.Instances:
                    List<clsObjectRel> searchConscious = null;
                    List<clsObjectRel> searchSubconscious = null;
                    if (advancedFilter_Direction == null)
                    {
                        searchConscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = FilterItemNote.GUID,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Other = advancedFilter_Object != null ? advancedFilter_Object.GUID : null,
                                ID_Parent_Other = advancedFilter_Class != null ? advancedFilter_Class.GUID : null
                            }
                        };

                        searchSubconscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Other = FilterItemNote.GUID,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Object = advancedFilter_Object != null ? advancedFilter_Object.GUID : null,
                                ID_Parent_Object = advancedFilter_Class != null ? advancedFilter_Class.GUID : null
                            }
                        };

                    }
                    else if (advancedFilter_Direction.GUID == globals.Direction_LeftRight.GUID)
                    {
                        searchConscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = FilterItemNote.GUID,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Other = advancedFilter_Object != null ? advancedFilter_Object.GUID : null,
                                ID_Parent_Other = advancedFilter_Class != null ? advancedFilter_Class.GUID : null
                            }
                        };
                    }
                    else if (advancedFilter_Direction.GUID == globals.Direction_RightLeft.GUID)
                    {
                        searchSubconscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Other = FilterItemNote.GUID,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Object = advancedFilter_Object != null ? advancedFilter_Object.GUID : null,
                                ID_Parent_Object = advancedFilter_Class != null ? advancedFilter_Class.GUID : null
                            }
                        };
                    }

                    var result = globals.LState_Nothing.Clone();
                    if (searchConscious != null)
                    {
                        result = dbReadConnector_AdvancedFilter_Conscious.GetDataObjectRel(searchConscious);

                    }

                    if (result.GUID != globals.LState_Error.GUID)
                    {
                        if (searchSubconscious != null)
                        {
                            result = dbReadConnector_AdvancedFilter_Subconscious.GetDataObjectRel(searchSubconscious);

                        }
                    }

                    if (result.GUID != globals.LState_Error.GUID)
                    {
                        if (advancedFilter_Direction == null)
                        {
                            InstanceViewItems = (from objectItem in InstanceViewItems
                                                 join relConscious in dbReadConnector_AdvancedFilter_Conscious.ObjectRels on objectItem.IdInstance equals relConscious.ID_Object
                                                 join relSubsconscious in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels on objectItem.IdInstance equals relSubsconscious.ID_Other
                                                 group objectItem by objectItem.IdInstance into objectItemGroup
                                                 select objectItemGroup.First()).ToList();
                        }
                        else if (advancedFilter_Direction.GUID == globals.Direction_LeftRight.GUID)
                        {
                            InstanceViewItems = (from objectItem in InstanceViewItems
                                                 join relConscious in dbReadConnector_AdvancedFilter_Conscious.ObjectRels on objectItem.IdInstance equals relConscious.ID_Object
                                                 group objectItem by objectItem.IdInstance into objectItemGroup
                                                 select objectItemGroup.First()).ToList();
                        }
                        else if (advancedFilter_Direction.GUID == globals.Direction_RightLeft.GUID)
                        {
                            InstanceViewItems = (from objectItem in InstanceViewItems
                                                 join relSubsconscious in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels on objectItem.IdInstance equals relSubsconscious.ID_Other
                                                 group objectItem by objectItem.IdInstance into objectItemGroup
                                                 select objectItemGroup.First()).ToList();
                        }
                        
                        Result_ListLoader = globals.LState_Success.Clone();
                    }
                    else
                    {
                        Result_ListLoader = globals.LState_Error.Clone();
                    }

                    break;
                case ListType.InstanceAttribute_Conscious:
                    
                    break;
                case ListType.InstanceInstance_Conscious:
                    
                    break;
                case ListType.InstanceInstance_Subconscious:
                    
                    break;
                case ListType.InstanceInstance_Omni:
                    
                    break;
                case ListType.InstanceOther_Conscious:
                    
                    break;
                case ListType.InstanceOther_Subconscious:
                    
                    break;
                default:
                    Result_ListLoader = globals.LState_Error.Clone();
                    break;
            }
        }

        public void Initialize_ClassList(clsOntologyItem filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemNote = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadClassList);
            threadListLoader.Start();
        }

        private void LoadClassList()
        {
            var result = dbReadConnector_Classes.GetDataClasses(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {

                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    ClassViewItems = (from classItem in dbReadConnector_Classes.Classes1.Where(classItem => classItem.Name == FilterItemNote.Name)
                                     join parentClassItem in dbReadConnector_Classes.Classes1 on classItem.GUID_Parent equals parentClassItem.GUID
                                     select new ClassViewItem(classItem, parentClassItem)).ToList();

                }
                else
                {
                    ClassViewItems = (from classItem in dbReadConnector_Classes.Classes1
                                      join parentClassItem in dbReadConnector_Classes.Classes1 on classItem.GUID_Parent equals parentClassItem.GUID
                                      select new ClassViewItem(classItem, parentClassItem)).ToList();
                }
            }
            else
            {
                ClassViewItems = new List<ClassViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_RelationTypeList(clsOntologyItem filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemNote = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadRelationTypeList);
            threadListLoader.Start();
        }

        private void LoadRelationTypeList()
        {
            var result = dbReadConnector_RelationType.GetDataRelationTypes(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {

                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    RelationTypeViewItems = (from relTypeItem in dbReadConnector_RelationType.RelationTypes.Where(classItem => classItem.Name == FilterItemNote.Name)
                                      select new RelationTypeViewItem(relTypeItem)).ToList();

                }
                else
                {
                    RelationTypeViewItems = (from relTypeItem in dbReadConnector_RelationType.RelationTypes
                                             select new RelationTypeViewItem(relTypeItem)).ToList();
                }
            }
            else
            {
                RelationTypeViewItems = new List<RelationTypeViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_AttributeTypeList(clsOntologyItem filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemNote = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadAttributeTypeList);
            threadListLoader.Start();
        }

        private void LoadAttributeTypeList()
        { 
            var result = dbReadConnector_AttributeTypes.GetDataAttributeType(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {
                
                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    AttributeTypeViewItems = (from attType in dbReadConnector_AttributeTypes.AttributeTypes.Where(attType => attType.Name == FilterItemNote.Name)
                                       join dataType in globals.DataTypes.DataTypes on attType.GUID_Parent equals dataType.GUID
                                       select new AttributeTypeViewItem(attType, dataType)).ToList();
                    
                }
                else
                {
                    AttributeTypeViewItems = (from attType in dbReadConnector_AttributeTypes.AttributeTypes
                                              join dataType in globals.DataTypes.DataTypes on attType.GUID_Parent equals dataType.GUID
                                              select new AttributeTypeViewItem(attType, dataType)).ToList();
                }
            }
            else
            {
                AttributeTypeViewItems = new List<AttributeTypeViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_InstanceList(clsOntologyItem filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemNote = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadInstanceList);
            threadListLoader.Start();
        }

        private void LoadInstanceList()
        {
            var result = dbReadConnector_Classes.GetDataClasses();
            if (result.GUID == globals.LState_Success.GUID)
            {
                result = dbReadConnector_Instance.GetDataObjects(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                    {
                        InstanceViewItems = (from instanceItem in dbReadConnector_Instance.Objects1.Where(objectItem => objectItem.Name == FilterItemNote.Name)
                                          join classItem in dbReadConnector_Classes.Classes1 on instanceItem.GUID_Parent equals classItem.GUID
                                          select new InstanceViewItem(instanceItem, classItem)).ToList();

                    }
                    else
                    {
                        InstanceViewItems = (from instanceItem in dbReadConnector_Instance.Objects1
                                             join classItem in dbReadConnector_Classes.Classes1 on instanceItem.GUID_Parent equals classItem.GUID
                                             select new InstanceViewItem(instanceItem, classItem)).ToList();
                        
                    }
                }
                else
                {
                    InstanceViewItems = new List<InstanceViewItem>();
                }

                
            }
            else
            {
                InstanceViewItems = new List<InstanceViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_ClassAttributeList(clsClassAtt filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemClassAtt = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadClassAttList);
            threadListLoader.Start();
        }

        private void LoadClassAttList()
        {
            var result = dbReadConnector_ClassAttributes.GetDataClassAtts(FilterItemClassAtt != null ? new List<clsClassAtt> { FilterItemClassAtt } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {
                
               
                if (Exact && FilterItemClassAtt != null && (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType)))
                {
                    ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts.Where(classAttItem => (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ? classAttItem.Name_AttributeType == FilterItemClassAtt.Name_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ? classAttItem.Name_Class == FilterItemClassAtt.Name_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType) ? classAttItem.Name_DataType == FilterItemClassAtt.Name_DataType : 1 == 1))
                                         select new ClassAttributeViewItem(classAttItem)).ToList();

                }
                else if (!Exact && FilterItemClassAtt != null && (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType)))
                {
                    ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts.Where(classAttItem => (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ? classAttItem.Name_AttributeType.ToLower().Contains(FilterItemClassAtt.Name_AttributeType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ? classAttItem.Name_Class.ToLower().Contains(FilterItemClassAtt.Name_Class.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType) ? classAttItem.Name_DataType.ToLower().Contains(FilterItemClassAtt.Name_DataType.ToLower()) : 1 == 1))
                                         select new ClassAttributeViewItem(classAttItem)).ToList();
                }
                else
                {
                    ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts
                                         select new ClassAttributeViewItem(classAttItem)).ToList();

                }
                
               

            }
            else
            {
                ClassAttViewItems = new List<ClassAttributeViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_ClassRelationList(clsClassRel filterItem, ListType listType, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemClassRel = filterItem;
            Exact = exact;
            this.listType = listType;

            threadListLoader = new Thread(LoadClassRelList);
            threadListLoader.Start();
        }


        private clsOntologyItem AddClassRelsToList(bool doOr)
        {
            var result = dbReadConnector_ClassRelations.GetDataClassRel(FilterItemClassRel != null ? new List<clsClassRel> { FilterItemClassRel } : null, doIds: false, doOr: doOr);
            if (result.GUID == globals.LState_Success.GUID)
            {


                if (Exact && FilterItemClassRel != null && (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Ontology) ||
                                                            FilterItemClassRel.Min_Forw != null ||
                                                            FilterItemClassRel.Max_Forw != null ||
                                                            FilterItemClassRel.Max_Backw != null))
                {

                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels.Where(classRelItem => (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ? classRelItem.Name_Class_Left == FilterItemClassRel.Name_Class_Left : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ? classRelItem.Name_Class_Right == FilterItemClassRel.Name_Class_Right : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ? classRelItem.Name_RelationType == FilterItemClassRel.Name_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Ontology) ? classRelItem.Ontology == FilterItemClassRel.Ontology : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Min_Forw != null ? classRelItem.Min_Forw == FilterItemClassRel.Min_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Forw != null ? classRelItem.Max_Forw == FilterItemClassRel.Max_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Backw != null ? classRelItem.Max_Backw == FilterItemClassRel.Max_Backw : 1 == 1)).ToList();

                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Omni:
                            ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                        
                    }



                }
                else if (!Exact && FilterItemClassRel != null && (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Ontology) ||
                                                            FilterItemClassRel.Min_Forw != null ||
                                                            FilterItemClassRel.Max_Forw != null ||
                                                            FilterItemClassRel.Max_Backw != null))
                {
                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels.Where(classRelItem => (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ? classRelItem.Name_Class_Left.ToLower().Contains(FilterItemClassRel.Name_Class_Left.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ? classRelItem.Name_Class_Right.ToLower().Contains(FilterItemClassRel.Name_Class_Right.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ? classRelItem.Name_RelationType.ToLower().Contains(FilterItemClassRel.Name_RelationType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Ontology) ? classRelItem.Ontology.ToLower().Contains(FilterItemClassRel.Ontology.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Min_Forw != null ? classRelItem.Min_Forw == FilterItemClassRel.Min_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Forw != null ? classRelItem.Max_Forw == FilterItemClassRel.Max_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Backw != null ? classRelItem.Max_Backw == FilterItemClassRel.Max_Backw : 1 == 1)).ToList();

                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;

                        case ListType.ClassClassRelation_Omni:
                            ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                    }
                }
                else
                {
                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels;
                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;

                        case ListType.ClassClassRelation_Omni:
                            ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                    }
                }



            }
            return result;
        }

        public clsOntologyItem SaveObjectAttributes(List<clsObjectAtt> objectAttributes)
        {
            var result = dbWriteConnector.SaveObjAtt(objectAttributes);

            return result;
        }

        public clsOntologyItem SaveObjectRelations(List<clsObjectRel> objectRelations)
        {
            var result = dbWriteConnector.SaveObjRel(objectRelations);

            return result;
        }

        private void LoadClassRelList()
        {
            Result_ListLoader = globals.LState_Nothing.Clone();
            ClassRelConciousViewItems = new List<ClassRelationConsciousViewItem>();
            ClassRelSubconsciousViewItems = new List<ClassRelationSubconsciousViewItem>();
            ClassRelOmniViewItems = new List<ClassRelationOmniViewItem>();

            clsOntologyItem result = globals.LState_Error.Clone();
            
            switch (listType)
            {
                case ListType.ClassClassRelation_Conscious:
                    result = AddClassRelsToList(false);
                    break;
                case ListType.ClassClassRelation_Subconscious:
                    result = AddClassRelsToList(false);
                    break;
                case ListType.ClassClassRelation_Omni:
                    result = AddClassRelsToList(false);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = AddClassRelsToList(true);
                    }
                    
                    break;
                case ListType.ClassOtherRelation_Conscious:
                    result = AddClassRelsToList(true);
                    break;
            }
           

            Result_ListLoader = result;
        }

        public void Initialize_ObjectAttributeList(clsObjectAtt filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }

            FilterItemObjectAttribute = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadObjAttList);
            threadListLoader.Start();
        }

        private void LoadObjAttList()
        {
            var result = dbReadConnector_ObjectAttributes.GetDataObjectAtt(FilterItemObjectAttribute != null ? new List<clsObjectAtt> { FilterItemObjectAttribute } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {


                if (Exact && FilterItemObjectAttribute != null && (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ||
                                                            FilterItemObjectAttribute.OrderID != null ||
                                                            FilterItemObjectAttribute.Val_Bit != null ||
                                                            FilterItemObjectAttribute.Val_Date != null ||
                                                            FilterItemObjectAttribute.Val_Double != null ||
                                                            FilterItemObjectAttribute.Val_Int != null) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Val_String))
                {
                    ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts.Where(objAtt => (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ? objAtt.ID_Attribute == FilterItemObjectAttribute.ID_Attribute : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ? objAtt.ID_AttributeType == FilterItemObjectAttribute.ID_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ? objAtt.ID_Class == FilterItemObjectAttribute.ID_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ? objAtt.ID_DataType == FilterItemObjectAttribute.ID_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ? objAtt.ID_Object == FilterItemObjectAttribute.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ? objAtt.Name_AttributeType == FilterItemObjectAttribute.Name_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ? objAtt.Name_Class == FilterItemObjectAttribute.Name_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ? objAtt.Name_DataType == FilterItemObjectAttribute.Name_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ? objAtt.Name_Object == FilterItemObjectAttribute.Name_Object : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.OrderID != null ? objAtt.OrderID == FilterItemObjectAttribute.OrderID : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Bit != null ? objAtt.Val_Bit == FilterItemObjectAttribute.Val_Bit : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Date != null ? objAtt.Val_Date == FilterItemObjectAttribute.Val_Date : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Double != null ? objAtt.Val_Double == FilterItemObjectAttribute.Val_Double : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Int != null ? objAtt.Val_Int == FilterItemObjectAttribute.Val_Int : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_String != null ? objAtt.Val_String.ToLower().Contains(FilterItemObjectAttribute.Val_String.ToLower()) : 1 == 1))
                                         select new ObjectAttributeViewItem(objectAttribute)).ToList();

                }
                else if (!Exact && FilterItemObjectAttribute != null && (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ||
                                                            FilterItemObjectAttribute.OrderID != null ||
                                                            FilterItemObjectAttribute.Val_Bit != null ||
                                                            FilterItemObjectAttribute.Val_Date != null ||
                                                            FilterItemObjectAttribute.Val_Double != null ||
                                                            FilterItemObjectAttribute.Val_Int != null) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Val_String))
                {
                    ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts.Where(objAtt => (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ? objAtt.ID_Attribute == FilterItemObjectAttribute.ID_Attribute : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ? objAtt.ID_AttributeType == FilterItemObjectAttribute.ID_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ? objAtt.ID_Class == FilterItemObjectAttribute.ID_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ? objAtt.ID_DataType == FilterItemObjectAttribute.ID_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ? objAtt.ID_Object == FilterItemObjectAttribute.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ? objAtt.Name_AttributeType.ToLower().Contains(FilterItemObjectAttribute.Name_AttributeType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ? objAtt.Name_Class.ToLower().Contains(FilterItemObjectAttribute.Name_Class.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ? objAtt.Name_DataType.ToLower().Contains(FilterItemObjectAttribute.Name_DataType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ? objAtt.Name_Object.ToLower().Contains(FilterItemObjectAttribute.Name_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.OrderID != null ? objAtt.OrderID == FilterItemObjectAttribute.OrderID : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Bit != null ? objAtt.Val_Bit == FilterItemObjectAttribute.Val_Bit : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Date != null ? objAtt.Val_Date == FilterItemObjectAttribute.Val_Date : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Double != null ? objAtt.Val_Double == FilterItemObjectAttribute.Val_Double : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Int != null ? objAtt.Val_Int == FilterItemObjectAttribute.Val_Int : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_String != null ? objAtt.Val_String.ToLower().Contains(FilterItemObjectAttribute.Val_String.ToLower()) : 1 == 1))
                                         select new ObjectAttributeViewItem(objectAttribute)).ToList();
                }
                else
                {
                    ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts
                                                select new ObjectAttributeViewItem(objectAttribute)).ToList();

                }



            }
            else
            {
                ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            }

            Result_ListLoader = result;
        }

        public void Initialize_ObjectRelationList(ListType listType, clsObjectRel filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            Result_ListLoader = globals.LState_Nothing.Clone();
            try
            {
                threadListLoader.Abort();
            }
            catch (Exception ex)
            {

            }
            this.listType = listType;
            FilterItemObjectRelation = filterItem;
            Exact = exact;

            threadListLoader = new Thread(LoadObjRelList);
            threadListLoader.Start();
        }

        private void LoadObjRelList()
        {
            var result = dbReadConnector_ObjectRelations.GetDataObjectRel(FilterItemObjectRelation != null ? new List<clsObjectRel> { FilterItemObjectRelation } : null);
            if (result.GUID == globals.LState_Success.GUID)
            {

                var objectRelationList = new List<clsObjectRel>();
                if (Exact && FilterItemObjectRelation != null && (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ||
                                                            FilterItemObjectRelation.OrderID != null))
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels.Where(objRel => (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ? objRel.ID_Object == FilterItemObjectRelation.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ? objRel.ID_Other == FilterItemObjectRelation.ID_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ? objRel.ID_Parent_Object == FilterItemObjectRelation.ID_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ? objRel.ID_Parent_Other == FilterItemObjectRelation.ID_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ? objRel.ID_RelationType == FilterItemObjectRelation.ID_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ? objRel.Name_Object == FilterItemObjectRelation.Name_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ? objRel.Name_Other == FilterItemObjectRelation.Name_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ? objRel.Name_Parent_Object == FilterItemObjectRelation.Name_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ? objRel.Name_Parent_Other == FilterItemObjectRelation.Name_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ? objRel.Name_RelationType == FilterItemObjectRelation.Name_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ? objRel.Ontology == FilterItemObjectRelation.Ontology : 1 == 1) &&
                                                                                                                       (FilterItemObjectRelation.OrderID != null ? objRel.OrderID == FilterItemObjectRelation.OrderID : 1 == 1)).ToList();

                }
                else if (!Exact && FilterItemObjectRelation != null && (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ||
                                                            FilterItemObjectRelation.OrderID != null))
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels.Where(objRel => (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ? objRel.ID_Object == FilterItemObjectRelation.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ? objRel.ID_Other == FilterItemObjectRelation.ID_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ? objRel.ID_Parent_Object == FilterItemObjectRelation.ID_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ? objRel.ID_Parent_Other == FilterItemObjectRelation.ID_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ? objRel.ID_RelationType == FilterItemObjectRelation.ID_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ? objRel.Name_Object.ToLower().Contains(FilterItemObjectRelation.Name_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ? objRel.Name_Other.ToLower().Contains(FilterItemObjectRelation.Name_Other.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ? objRel.Name_Parent_Object.ToLower().Contains(FilterItemObjectRelation.Name_Parent_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ? objRel.Name_Parent_Other.ToLower().Contains(FilterItemObjectRelation.Name_Parent_Other.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ? objRel.Name_RelationType.ToLower().Contains(FilterItemObjectRelation.Name_RelationType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ? objRel.Ontology.ToLower().Contains(FilterItemObjectRelation.Ontology.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemObjectRelation.OrderID != null ? objRel.OrderID == FilterItemObjectRelation.OrderID : 1 == 1)).ToList();
                }
                else
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels;
                                                

                }

                switch (listType)
                {
                    case ListType.InstanceInstance_Conscious:
                        ObjectObjectConsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Conscious(objRel)).ToList();
                        break;
                    case ListType.InstanceInstance_Subconscious:
                        ObjectObjectSubconsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Subconscious(objRel)).ToList();
                        break;
                    case ListType.InstanceInstance_Omni:
                        ObjectObjectOmniViewItems = objectRelationList.Select(objRel => new ObjectObject_Omni(objRel)).ToList();
                        break;
                    case ListType.InstanceOther_Conscious:
                        ObjectOtherConsciousViewItems = objectRelationList.Where(objRel => objRel.Ontology != globals.Type_Object).Select(objRel => new ObjectObject_Conscious(objRel)).ToList();
                        break;
                    case ListType.InstanceOther_Subconscious:
                        ObjectOtherSubconsciousViewItems = objectRelationList.Where(objRel => objRel.Ontology != globals.Type_Object).Select(objRel => new ObjectObject_Subconscious(objRel)).ToList();
                        break;
                }
                

            }
            else
            {
                ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            }

            Result_ListLoader = result;
        }

        private void Initialize()
        {
            dbReadConnector_AttributeTypes = new OntologyModDBConnector(globals);
            dbReadConnector_Classes = new OntologyModDBConnector(globals);
            dbReadConnector_RelationType = new OntologyModDBConnector(globals);
            dbReadConnector_Instance = new OntologyModDBConnector(globals);
            dbReadConnector_ClassAttributes = new OntologyModDBConnector(globals);
            dbReadConnector_ClassRelations = new OntologyModDBConnector(globals);
            dbReadConnector_ObjectAttributes = new OntologyModDBConnector(globals);
            dbReadConnector_ObjectRelations = new OntologyModDBConnector(globals);
            dbReadConnector_AdvancedFilter_Conscious = new OntologyModDBConnector(globals);
            dbReadConnector_AdvancedFilter_Subconscious = new OntologyModDBConnector(globals);
            dbReadConnector_OItem = new OntologyModDBConnector(globals);
            dbWriteConnector = new OntologyModDBConnector(globals);
        }
    }
}
