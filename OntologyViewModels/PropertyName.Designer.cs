﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OntologyViewModels {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PropertyName {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PropertyName() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OntologyViewModels.PropertyName", typeof(PropertyName).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AddAllowed.
        /// </summary>
        public static string OItemList_AddAllowed {
            get {
                return ResourceManager.GetString("OItemList_AddAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AdvancedFilter_Class.
        /// </summary>
        public static string OItemList_AdvancedFilter_Class {
            get {
                return ResourceManager.GetString("OItemList_AdvancedFilter_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AdvancedFilter_Direction.
        /// </summary>
        public static string OItemList_AdvancedFilter_Direction {
            get {
                return ResourceManager.GetString("OItemList_AdvancedFilter_Direction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AdvancedFilter_Object.
        /// </summary>
        public static string OItemList_AdvancedFilter_Object {
            get {
                return ResourceManager.GetString("OItemList_AdvancedFilter_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AdvancedFilter_RelationType.
        /// </summary>
        public static string OItemList_AdvancedFilter_RelationType {
            get {
                return ResourceManager.GetString("OItemList_AdvancedFilter_RelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdAttribute.
        /// </summary>
        public static string OItemList_Attribute_IdAttribute {
            get {
                return ResourceManager.GetString("OItemList_Attribute_IdAttribute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeIdAttributeType.
        /// </summary>
        public static string OItemList_Attribute_IdAttributeType {
            get {
                return ResourceManager.GetString("OItemList_Attribute_IdAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeIdClass.
        /// </summary>
        public static string OItemList_Attribute_IdClass {
            get {
                return ResourceManager.GetString("OItemList_Attribute_IdClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeIdDataType.
        /// </summary>
        public static string OItemList_Attribute_IdDataType {
            get {
                return ResourceManager.GetString("OItemList_Attribute_IdDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeIdObject.
        /// </summary>
        public static string OItemList_Attribute_IdObject {
            get {
                return ResourceManager.GetString("OItemList_Attribute_IdObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeNameAttributeType.
        /// </summary>
        public static string OItemList_Attribute_NameAttributeType {
            get {
                return ResourceManager.GetString("OItemList_Attribute_NameAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeNameClass.
        /// </summary>
        public static string OItemList_Attribute_NameClass {
            get {
                return ResourceManager.GetString("OItemList_Attribute_NameClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeNameDataType.
        /// </summary>
        public static string OItemList_Attribute_NameDataType {
            get {
                return ResourceManager.GetString("OItemList_Attribute_NameDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeNameObject.
        /// </summary>
        public static string OItemList_Attribute_NameObject {
            get {
                return ResourceManager.GetString("OItemList_Attribute_NameObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeOrderId.
        /// </summary>
        public static string OItemList_Attribute_OrderId {
            get {
                return ResourceManager.GetString("OItemList_Attribute_OrderId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueBool.
        /// </summary>
        public static string OItemList_Attribute_ValueBool {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueBool", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueDate.
        /// </summary>
        public static string OItemList_Attribute_ValueDate {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueDouble.
        /// </summary>
        public static string OItemList_Attribute_ValueDouble {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueDouble", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueInt.
        /// </summary>
        public static string OItemList_Attribute_ValueInt {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueInt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueNamed.
        /// </summary>
        public static string OItemList_Attribute_ValueNamed {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueNamed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ValueString.
        /// </summary>
        public static string OItemList_Attribute_ValueString {
            get {
                return ResourceManager.GetString("OItemList_Attribute_ValueString", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeTypeIdAttributeType.
        /// </summary>
        public static string OItemList_AttributeType_IdAttributeType {
            get {
                return ResourceManager.GetString("OItemList_AttributeType_IdAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeTypeIdDataType.
        /// </summary>
        public static string OItemList_AttributeType_IdDataType {
            get {
                return ResourceManager.GetString("OItemList_AttributeType_IdDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeTypeNameAttributeType.
        /// </summary>
        public static string OItemList_AttributeType_NameAttributeType {
            get {
                return ResourceManager.GetString("OItemList_AttributeType_NameAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameDataType.
        /// </summary>
        public static string OItemList_AttributeType_NameDataType {
            get {
                return ResourceManager.GetString("OItemList_AttributeType_NameDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassIdClass.
        /// </summary>
        public static string OItemList_Class_IdClass {
            get {
                return ResourceManager.GetString("OItemList_Class_IdClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdParentClass.
        /// </summary>
        public static string OItemList_Class_IdParentClass {
            get {
                return ResourceManager.GetString("OItemList_Class_IdParentClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameClass.
        /// </summary>
        public static string OItemList_Class_NameClass {
            get {
                return ResourceManager.GetString("OItemList_Class_NameClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameParentClass.
        /// </summary>
        public static string OItemList_Class_NameParentClass {
            get {
                return ResourceManager.GetString("OItemList_Class_NameParentClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttIdAttributeType.
        /// </summary>
        public static string OItemList_ClassAtt_IdAttributeType {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_IdAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttIdClass.
        /// </summary>
        public static string OItemList_ClassAtt_IdClass {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_IdClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttIdDataType.
        /// </summary>
        public static string OItemList_ClassAtt_IdDataType {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_IdDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max.
        /// </summary>
        public static string OItemList_ClassAtt_Max {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_Max", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Min.
        /// </summary>
        public static string OItemList_ClassAtt_Min {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_Min", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttNameAttributeType.
        /// </summary>
        public static string OItemList_ClassAtt_NameAttributeType {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_NameAttributeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttNameClass.
        /// </summary>
        public static string OItemList_ClassAtt_NameClass {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_NameClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttNameDataTpye.
        /// </summary>
        public static string OItemList_ClassAtt_NameDataTpye {
            get {
                return ResourceManager.GetString("OItemList_ClassAtt_NameDataTpye", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttFilter.
        /// </summary>
        public static string OItemList_ClassAttFilter {
            get {
                return ResourceManager.GetString("OItemList_ClassAttFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdClassLeft.
        /// </summary>
        public static string OItemList_ClassRel_IdClassLeft {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_IdClassLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdClassRight.
        /// </summary>
        public static string OItemList_ClassRel_IdClassRight {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_IdClassRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelIdRelationType.
        /// </summary>
        public static string OItemList_ClassRel_IdRelationType {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_IdRelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max_Backw.
        /// </summary>
        public static string OItemList_ClassRel_Max_Backw {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_Max_Backw", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max_Forw.
        /// </summary>
        public static string OItemList_ClassRel_Max_Forw {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_Max_Forw", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Min_Forw.
        /// </summary>
        public static string OItemList_ClassRel_Min_Forw {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_Min_Forw", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameClassLeft.
        /// </summary>
        public static string OItemList_ClassRel_NameClassLeft {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_NameClassLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameClassRight.
        /// </summary>
        public static string OItemList_ClassRel_NameClassRight {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_NameClassRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelNameRelationType.
        /// </summary>
        public static string OItemList_ClassRel_NameRelationType {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_NameRelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ontology.
        /// </summary>
        public static string OItemList_ClassRel_Ontology {
            get {
                return ResourceManager.GetString("OItemList_ClassRel_Ontology", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelFilter.
        /// </summary>
        public static string OItemList_ClassRelFilter {
            get {
                return ResourceManager.GetString("OItemList_ClassRelFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CountItems.
        /// </summary>
        public static string OItemList_CountItems {
            get {
                return ResourceManager.GetString("OItemList_CountItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DelAllowed.
        /// </summary>
        public static string OItemList_DelAllowed {
            get {
                return ResourceManager.GetString("OItemList_DelAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdSelected.
        /// </summary>
        public static string OItemList_IdSelected {
            get {
                return ResourceManager.GetString("OItemList_IdSelected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to InstanceIdClass.
        /// </summary>
        public static string OItemList_Instance_IdClass {
            get {
                return ResourceManager.GetString("OItemList_Instance_IdClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IdInstance.
        /// </summary>
        public static string OItemList_Instance_IdInstance {
            get {
                return ResourceManager.GetString("OItemList_Instance_IdInstance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to InstanceNameClass.
        /// </summary>
        public static string OItemList_Instance_NameClass {
            get {
                return ResourceManager.GetString("OItemList_Instance_NameClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NameInstance.
        /// </summary>
        public static string OItemList_Instance_NameInstance {
            get {
                return ResourceManager.GetString("OItemList_Instance_NameInstance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMaxBackwEdit.
        /// </summary>
        public static string OItemList_IsAllowedMaxBackwEdit {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMaxBackwEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMaxDecrease.
        /// </summary>
        public static string OItemList_IsAllowedMaxDecrease {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMaxDecrease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMaxEdit.
        /// </summary>
        public static string OItemList_IsAllowedMaxEdit {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMaxEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMaxIncrease.
        /// </summary>
        public static string OItemList_IsAllowedMaxIncrease {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMaxIncrease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMaxToInvinite.
        /// </summary>
        public static string OItemList_IsAllowedMaxToInvinite {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMaxToInvinite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMinDecrease.
        /// </summary>
        public static string OItemList_IsAllowedMinDecrease {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMinDecrease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMinEdit.
        /// </summary>
        public static string OItemList_IsAllowedMinEdit {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMinEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsAllowedMinIncrease.
        /// </summary>
        public static string OItemList_IsAllowedMinIncrease {
            get {
                return ResourceManager.GetString("OItemList_IsAllowedMinIncrease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AttributeTypeViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_AttributeTypeViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_AttributeTypeViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassAttViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ClassAttViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ClassAttViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelConsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ClassRelConsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ClassRelConsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelOmniViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ClassRelOmniViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ClassRelOmniViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassRelSubconsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ClassRelSubconsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ClassRelSubconsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClassViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ClassViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ClassViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to InstanceViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_InstanceViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_InstanceViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ListAdapter_ListLoadState.
        /// </summary>
        public static string OItemList_ListAdapter_ListLoadState {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ListLoadState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectAttributeViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectAttributeViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectAttributeViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectObjectConsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectObjectConsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectObjectConsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectObjectOmniViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectObjectOmniViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectObjectOmniViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectObjectSubconsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectObjectSubconsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectObjectSubconsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectOtherConsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectOtherConsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectOtherConsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectOtherSubconsciousViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_ObjectOtherSubconsciousViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_ObjectOtherSubconsciousViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RelationTypeViewItems.
        /// </summary>
        public static string OItemList_ListAdapter_RelationTypeViewItems {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_RelationTypeViewItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Result_ListLoader.
        /// </summary>
        public static string OItemList_ListAdapter_Result_ListLoader {
            get {
                return ResourceManager.GetString("OItemList_ListAdapter_Result_ListLoader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ListError.
        /// </summary>
        public static string OItemList_ListError {
            get {
                return ResourceManager.GetString("OItemList_ListError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ListType.
        /// </summary>
        public static string OItemList_ListType {
            get {
                return ResourceManager.GetString("OItemList_ListType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NoteList.
        /// </summary>
        public static string OItemList_NoteList {
            get {
                return ResourceManager.GetString("OItemList_NoteList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NoteListFilter.
        /// </summary>
        public static string OItemList_NoteListFilter {
            get {
                return ResourceManager.GetString("OItemList_NoteListFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_ID_Object.
        /// </summary>
        public static string OItemList_ObjectRel_ID_Object {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_ID_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_ID_Other.
        /// </summary>
        public static string OItemList_ObjectRel_ID_Other {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_ID_Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_ID_Parent_Object.
        /// </summary>
        public static string OItemList_ObjectRel_ID_Parent_Object {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_ID_Parent_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_ID_Parent_Other.
        /// </summary>
        public static string OItemList_ObjectRel_ID_Parent_Other {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_ID_Parent_Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_ID_RelationType.
        /// </summary>
        public static string OItemList_ObjectRel_ID_RelationType {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_ID_RelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Name_Object.
        /// </summary>
        public static string OItemList_ObjectRel_Name_Object {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Name_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Name_Other.
        /// </summary>
        public static string OItemList_ObjectRel_Name_Other {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Name_Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Name_Parent_Object.
        /// </summary>
        public static string OItemList_ObjectRel_Name_Parent_Object {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Name_Parent_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Name_Parent_Other.
        /// </summary>
        public static string OItemList_ObjectRel_Name_Parent_Other {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Name_Parent_Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Name_RelationType.
        /// </summary>
        public static string OItemList_ObjectRel_Name_RelationType {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Name_RelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_Ontology.
        /// </summary>
        public static string OItemList_ObjectRel_Ontology {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_Ontology", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ObjectRel_OrderID.
        /// </summary>
        public static string OItemList_ObjectRel_OrderID {
            get {
                return ResourceManager.GetString("OItemList_ObjectRel_OrderID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RelationTypeIdRelationType.
        /// </summary>
        public static string OItemList_RelationType_IdRelationType {
            get {
                return ResourceManager.GetString("OItemList_RelationType_IdRelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RelationTypeNameRelationType.
        /// </summary>
        public static string OItemList_RelationType_NameRelationType {
            get {
                return ResourceManager.GetString("OItemList_RelationType_NameRelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ViewModel_ListLoadState.
        /// </summary>
        public static string OItemList_ViewModel_ListLoadState {
            get {
                return ResourceManager.GetString("OItemList_ViewModel_ListLoadState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ViewModelErrorType.
        /// </summary>
        public static string OItemList_ViewModelErrorType {
            get {
                return ResourceManager.GetString("OItemList_ViewModelErrorType", resourceCulture);
            }
        }
    }
}
